/* The tse_mac_device structure needs to be set to correspond with the ip names you are using in your design. */
/* There are some common projects used with this example that use the names below, so these are included in this file. */
/* If you are using one of these projects then no mods will be necessary. */
/* Otherwise you can remove all the tse_mac_device[MAXNETS] = statements below and add the correct one for your project. */
/* The names to use can be found in the BSP system.h file. */

/* If the triple speed ethernet ip is not in your design then this entire file is not necessary and is excluded. */
#ifdef ALTERA_TRIPLE_SPEED_MAC
#ifdef ALT_INICHE
    #include "ipport.h"
#endif

#include "system.h"
#include "altera_avalon_tse_system_info.h"

/* if using marvell phy and you want rgmii mode, uncomment this define */
/*#define SET_MARVELL_RGMII_MODE*/

#ifdef SET_MARVELL_RGMII_MODE
   #define ADDITIONAL_PHY_CFG  &marvell_cfg_rgmii
#else
   #define ADDITIONAL_PHY_CFG  0
#endif   

#ifdef TSE_MAC_BASE
alt_tse_system_info tse_mac_device[MAXNETS] = {
			TSE_SYSTEM_EXT_MEM_NO_SHARED_FIFO(TSE_MAC, 0, MSGDMA_TX, MSGDMA_RX, TSE_PHY_AUTO_ADDRESS, ADDITIONAL_PHY_CFG, DESCRIPTOR_MEMORY)
};

#else

#ifdef DESCRIPTOR_MEMORY_BASE
alt_tse_system_info tse_mac_device[MAXNETS] = {
		TSE_SYSTEM_EXT_MEM_NO_SHARED_FIFO(TSE_0_TSE, 0, TSE_0_DMA_TX, TSE_0_DMA_RX, TSE_PHY_AUTO_ADDRESS, ADDITIONAL_PHY_CFG, DESCRIPTOR_MEMORY)
		
};
#else
alt_tse_system_info tse_mac_device[MAXNETS] = {
		TSE_SYSTEM_INT_MEM_NO_SHARED_FIFO(TSE_0_TSE, 0, TSE_0_DMA_TX, TSE_0_DMA_RX, TSE_PHY_AUTO_ADDRESS, ADDITIONAL_PHY_CFG)
		
};
#endif

#endif
#endif

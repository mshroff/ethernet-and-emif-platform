#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <time.h>
#include <arpa/inet.h>
#include <unistd.h>

void error(char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int DEPTH;
    int sock, portno, n;
    
    struct sockaddr_in serv_addr;
    struct hostent *server;
    struct timespec t1, t0;
    
    char buffer[256];
    if (argc < 3) {
        fprintf(stderr,"usage %s hostname port\n", argv[0]);
        exit(0);
    }
    DEPTH = atoi(argv[3]);
    portno = atoi(argv[2]);
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sock,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
    error("ERROR connecting");
    char menu_buffer[5024] = {0};
    read(sock, &menu_buffer,1024);
    char acquire_text[1024] = "acquire\n";
    send(sock, &acquire_text,sizeof(acquire_text),0);
    usleep(1000000);
    int data_size = DEPTH;
    send(sock, &data_size, sizeof(data_size),0);
    usleep(1000000);
    
    float send_buff[DEPTH];
    float rec_buff;
    clock_t begin = clock();
    
    
    clock_gettime(CLOCK_REALTIME,&t0);
    //printf("start clock");
    for (int i=0; i<data_size; i++)
    {
        send_buff[i] = (float) i;
        //printf("%f\n", send_buff);
    }
    int sent_bytes = send(sock, &send_buff, sizeof(send_buff),0);
    printf("Bytes sent: %d\n",sent_bytes);
    
    
    char closed;
    int closed_check = 1;
    while(closed_check = read(sock, &closed,1024) != 0)
    {
    }
    clock_t end = clock();
    clock_gettime(CLOCK_REALTIME,&t1);
    float elapsed = ((float)end - (float) begin)/CLOCKS_PER_SEC;
    
    float elapsed1 = (t1.tv_sec-t0.tv_sec) + (t1.tv_nsec-t0.tv_nsec)/1E9;
    //printf("[%ld, %ld],\n", begin, end);
    //printf("%f\n",elapsed);
    printf("Time taken for transfer: %f\n, ",elapsed1);            //MAKE SURE PRINT IS OFF ON JTAG SERVER
    //printf("%ld", t1.tv_sec); */
    usleep(500000);
    return 0;
}


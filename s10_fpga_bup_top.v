
module s10_fpga_bup_top
    (
    input            clk_50m_s10        , // System clock, 50MHz
    input            clk_s10top_125m_p  , // Clock 125MHz from U6, Bank #3J
    input            cpu_resetn         , // FPGA reset, low active
    output           enet_sgmii_tx_p    , // SGMII interface
    input            enet_sgmii_rx_p    , // SGMII interface
    output           enet_rstn          , // SGMII interface
    input            enet_intn          , // SGMII interface
    output           enet_mdc           , // SGMII interface
    inout            enet_mdio          , // SGMII interface
    output  [3:0]    user_led_g         , // User LEDs Green
	output  [3:0]    user_led_r         , // User LEDs Red
    input   [2:0]    user_pb            , // User push buttons
    inout            i2c_1v8_scl        , // SCL
    inout            i2c_1v8_sda        , // SDA
    output  [26:0]   fm_a               , // CFI Flash interface, fm_a[27:2] ~ "FM_A[26:1] on schematic"
    inout   [15:0]   fm_d               , // CFI Flash interface
    output           flash_wen          , // CFI Flash interface
    output           flash_oen          , // CFI Flash interface
    output  [1:0]    flash_cen          , // CFI Flash interface
    output           flash_advn         , // CFI Flash interface
    output           flash_clk          , // CFI Flash interface
    output           flash_resetn       , // CFI Flash interface
    input   [1:0]    flash_rdybsyn      , // CFI Flash interface, not used yet
	 output wire [0:0]  emif_s10_0_mem_mem_ck,               //         emif_s10_0_mem.emif_s10_0_mem_mem_ck
	output wire [0:0]  emif_s10_0_mem_mem_ck_n,             //                       .emif_s10_0_mem_mem_ck_n
	output wire [14:0] emif_s10_0_mem_mem_a,                //                       .emif_s10_0_mem_mem_a
	output wire [2:0]  emif_s10_0_mem_mem_ba,               //                       .emif_s10_0_mem_mem_ba
	output wire [0:0]  emif_s10_0_mem_mem_cke,              //                       .emif_s10_0_mem_mem_cke
	output wire [0:0]  emif_s10_0_mem_mem_cs_n,             //                       .emif_s10_0_mem_mem_cs_n
	output wire [0:0]  emif_s10_0_mem_mem_odt,              //                       .emif_s10_0_mem_mem_odt
	output wire [0:0]  emif_s10_0_mem_mem_reset_n,          //                       .emif_s10_0_mem_mem_reset_n
	output wire [0:0]  emif_s10_0_mem_mem_we_n,             //                       .emif_s10_0_mem_mem_we_n
	output wire [0:0]  emif_s10_0_mem_mem_ras_n,            //                       .emif_s10_0_mem_mem_ras_n
	output wire [0:0]  emif_s10_0_mem_mem_cas_n,            //                       .emif_s10_0_mem_mem_cas_n
	inout  wire [3:0]  emif_s10_0_mem_mem_dqs,              //                       .emif_s10_0_mem_mem_dqs
	inout  wire [3:0]  emif_s10_0_mem_mem_dqs_n,            //                       .emif_s10_0_mem_mem_dqs_n
	inout  wire [31:0] emif_s10_0_mem_mem_dq,               //                       .emif_s10_0_mem_mem_dq
	output wire [3:0]  emif_s10_0_mem_mem_dm,               //                       .emif_s10_0_mem_mem_dm
	input  wire        emif_s10_0_oct_oct_rzqin,            //         emif_s10_0_oct.oct_rzqin
	output wire        emif_s10_0_status_local_cal_success_LED, //      emif_s10_0_status.local_cal_success
	output wire        emif_s10_0_status_local_cal_fail_LED,    //                       .local_cal_fail
	input  wire        local_reset_req,                     //        local_reset_req.local_reset_req
	output wire        local_reset_done_LED,                     //     local_reset_status.local_reset_done
	output wire			 local_reset_not_done_LED,
	input wire 			CLK133 
    );

//Reset
localparam RESET_SOURCES_COUNT = 2;

reg [(RESET_SOURCES_COUNT - 1):0] resetn_sources;
wire            global_resetn;
wire            reset_phy_clk_n_from_the_ddr3_top;
wire            global_reset_n_to_the_ddr3_top;

//glue logic
reg  [25:0]     counter;
reg             heartbeat_led;
wire [7:0]      user_led_tmp; 


// DDR3 wires
wire ddr3_local_reset_done;
wire ddr3_status_local_cal_fail;
wire ddr3_status_local_cal_success;

wire ddr3_local_reset_req;
// Ethernet interface assignments
wire            mdio_in;
wire            mdio_oen;
wire            mdio_out;
wire            flash_select;
wire            flash_cen_temp;
wire            enet_resetn;

assign mdio_in = enet_mdio;
assign enet_mdio = (!mdio_oen) ? mdio_out : 1'bz;
assign enet_rstn = enet_resetn;
//io_buffer   io_buffer_inst0(
//            .din           (mdio_out ), //    din.export
//            .dout          (mdio_in  ), //   dout.export
//            .oe            (!mdio_oen), //     oe.export
//            .pad_io        (enet_mdio)  // pad_io.export
//            );

assign flash_resetn = enet_resetn; // flash ready after FPGA is configured, reset during configuration
assign flash_advn   = 1'b0;
assign flash_clk    = 1'b0;

 always @(posedge clk_50m_s10 or negedge global_resetn)
    begin
      if (global_resetn == 1'b0)
          counter <= 26'd0;
      else if (counter == 26'd49999999)
          counter <= 26'd0;
	  else
	      counter <= counter + 1'd1;
    end
	
 always @(posedge clk_50m_s10 or negedge global_resetn)
    begin
      if (global_resetn == 1'b0)
        heartbeat_led <= 1'b0;  
      else if (counter == 26'd49999999)
        heartbeat_led <= !heartbeat_led;          
    end

assign user_led_g = {heartbeat_led,user_led_tmp[6:4]};
assign user_led_r = user_led_tmp[3:0];

//
// Declare a localparam for the number of reset sources that exist in this design.
// This parameter will be used by the global_reset_generator module.
//
// Tie the reset sources from the system into the global_reset_generator module.
// The reset counter width of 16 should provide a 2^16 clock assertion of global reset
// which at 50MHz should be 1.31ms long.
//
//assign global_reset_n_to_the_ddr3_top =  1'b1; // don't need another reset source here
//
//always @ (*) begin
//    resetn_sources[(RESET_SOURCES_COUNT - 1)]   <=  cpu_resetn;
//    //resetn_sources[(RESET_SOURCES_COUNT - 2)]   <=  reset_phy_clk_n_from_the_ddr3_top;
//    resetn_sources[(RESET_SOURCES_COUNT - 2)]   <=  1'b1; // don't need another reset source here
//end
//
//global_reset_generator 
//#(
//    .RESET_SOURCES_WIDTH  (RESET_SOURCES_COUNT),
//    .RESET_COUNTER_WIDTH  (16)
//) global_reset_generator_inst
//(
//    .clk            (clk_50m_s10),
//    .resetn_sources (resetn_sources),
//    .global_resetn  (global_resetn),
//    .pll_resetn     (global_reset_n_to_the_ddr3_top)
//);

reset_generator reset_generator_i
                (
                .i_clk                                    (clk_50m_s10         ), // input clock
                .i_reset_n                                (cpu_resetn          ), // input reset, low active
                .o_reset_n_0                              (enet_resetn         ), // output reset #0, low active, de-asserted first
                .o_reset_n_1                              (global_resetn       )  // output reset #1, low active, de-asserted later
                );

bup_qsys    bup_qsys_inst (   
	.bup_qsys_cfi_flash_atb_bridge_0_out_tcm_address_out      (fm_a                ),
	.bup_qsys_cfi_flash_atb_bridge_0_out_tcm_read_n_out       (flash_oen           ),
	.bup_qsys_cfi_flash_atb_bridge_0_out_tcm_write_n_out      (flash_wen           ),
	.bup_qsys_cfi_flash_atb_bridge_0_out_tcm_data_out         (fm_d                ),
	.bup_qsys_cfi_flash_atb_bridge_0_out_tcm_chipselect_n_out (flash_cen_temp      ),
    .opencores_i2c_export_0_scl_pad_io                        (i2c_1v8_scl         ),
    .opencores_i2c_export_0_sda_pad_io                        (i2c_1v8_sda         ),
    .button_pio_external_connection_export                    (user_pb             ),
	.flash_select_external_connection_export                  (flash_select        ),
	.led_pio_out_export                                       (user_led_tmp        ),
	.reset_1_reset_n                                          (global_resetn       ),
	.reset_125m_reset_n                                       (global_resetn       ),                   
	.clk_125m_clk                                             (clk_s10top_125m_p   ),
	.sys_clk_clk                                              (clk_50m_s10         ),
	.tse_mac_mac_mdio_connection_mdc                          (enet_mdc            ),
	.tse_mac_mac_mdio_connection_mdio_in                      (mdio_in             ),
	.tse_mac_mac_mdio_connection_mdio_out                     (mdio_out            ),
	.tse_mac_mac_mdio_connection_mdio_oen                     (mdio_oen            ),
	.tse_mac_serial_connection_rxp_0                          (enet_sgmii_rx_p     ),
	.tse_mac_serial_connection_txp_0                          (enet_sgmii_tx_p     ),  
	.tse_mac_misc_connection_magic_sleep_n                    (1'b1                ),   
	.tse_mac_misc_connection_ff_tx_crc_fwd                    (1'b0                ),
	.tse_mac_misc_connection_magic_wakeup                     (                    ),
	.tse_mac_misc_connection_ff_tx_septy                      (                    ),
	.tse_mac_misc_connection_tx_ff_uflow                      (                    ),
	.tse_mac_misc_connection_ff_tx_a_full                     (                    ),
	.tse_mac_misc_connection_ff_tx_a_empty                    (                    ),
	.tse_mac_misc_connection_rx_err_stat                      (                    ),
	.tse_mac_misc_connection_rx_frm_type                      (                    ),
	.tse_mac_misc_connection_ff_rx_dsav                       (                    ),
	.tse_mac_misc_connection_ff_rx_a_full                     (                    ),
	.tse_mac_misc_connection_ff_rx_a_empty                    (                    ),
	.tse_mac_status_led_connection_link                       (                    ),
	.tse_mac_status_led_connection_an                         (                    ),
	.tse_mac_status_led_connection_char_err                   (                    ),
	.tse_mac_status_led_connection_disp_err                   (                    ),
	.tse_mac_status_led_connection_crs                        (                    ),
	.tse_mac_status_led_connection_col                        (                    ),
	.tse_mac_status_led_connection_panel_link                 (                    ),
	.emif_s10_0_mem_mem_ck              (emif_s10_0_mem_mem_ck),                                      //  output,    width = 1,                mem.emif_s10_0_mem_mem_ck
	.emif_s10_0_mem_mem_ck_n            (emif_s10_0_mem_mem_ck_n),                                    //  output,    width = 1,                   .emif_s10_0_mem_mem_ck_n
	.emif_s10_0_mem_mem_a               (emif_s10_0_mem_mem_a),                                       //  output,   width = 15,                   .emif_s10_0_mem_mem_a
	.emif_s10_0_mem_mem_ba              (emif_s10_0_mem_mem_ba),                                      //  output,    width = 3,                   .emif_s10_0_mem_mem_ba
	.emif_s10_0_mem_mem_cke             (emif_s10_0_mem_mem_cke),                                     //  output,    width = 1,                   .emif_s10_0_mem_mem_cke
	.emif_s10_0_mem_mem_cs_n            (emif_s10_0_mem_mem_cs_n),                                    //  output,    width = 1,                   .emif_s10_0_mem_mem_cs_n
	.emif_s10_0_mem_mem_odt             (emif_s10_0_mem_mem_odt),                                     //  output,    width = 1,                   .emif_s10_0_mem_mem_odt
	.emif_s10_0_mem_mem_reset_n         (emif_s10_0_mem_mem_reset_n),                                 //  output,    width = 1,                   .emif_s10_0_mem_mem_reset_n
	.emif_s10_0_mem_mem_we_n            (emif_s10_0_mem_mem_we_n),                                    //  output,    width = 1,                   .emif_s10_0_mem_mem_we_n
	.emif_s10_0_mem_mem_ras_n           (emif_s10_0_mem_mem_ras_n),                                   //  output,    width = 1,                   .emif_s10_0_mem_mem_ras_n
	.emif_s10_0_mem_mem_cas_n           (emif_s10_0_mem_mem_cas_n),                                   //  output,    width = 1,                   .emif_s10_0_mem_mem_cas_n
	.emif_s10_0_mem_mem_dqs             (emif_s10_0_mem_mem_dqs),                                     //   inout,    width = 9,                   .emif_s10_0_mem_mem_dqs
	.emif_s10_0_mem_mem_dqs_n           (emif_s10_0_mem_mem_dqs_n),                                   //   inout,    width = 9,                   .emif_s10_0_mem_mem_dqs_n
	.emif_s10_0_mem_mem_dq              (emif_s10_0_mem_mem_dq),                                      //   inout,   width = 72,                   .emif_s10_0_mem_mem_dq
	.emif_s10_0_mem_mem_dm              (emif_s10_0_mem_mem_dm),                                      //  output,    width = 9,                   .emif_s10_0_mem_mem_dm
	.emif_s10_0_oct_oct_rzqin           (emif_s10_0_oct_oct_rzqin),                                   //   input,    width = 1,                oct.oct_rzqin
	
	.emif_s10_0_status_local_cal_success   (ddr3_status_local_cal_success),                        //  output,    width = 1,             status.local_cal_success
	.emif_s10_0_status_local_cal_fail      (ddr3_status_local_cal_fail),
	.clk_133_clk							(CLK133),
	.emif_s10_0_local_reset_req_local_reset_req	 (ddr3_local_reset_req),
	.emif_s10_0_local_reset_status_local_reset_done (ddr3_local_reset_done),
	.cal_success_external_connection_export		(ddr3_status_local_cal_success),
	.cal_fail_external_connection_export			(ddr3_status_local_cal_fail),
	.reset_done_external_connection_export			(ddr3_local_reset_done),
	.local_reset_req_external_connection_export		(ddr3_local_reset_req)
	); 

assign  flash_cen[0]    = flash_cen_temp | flash_select;
assign  flash_cen[1]    = flash_cen_temp | (!flash_select);	

assign 	emif_s10_0_status_local_cal_success_LED  = ~ddr3_status_local_cal_success;
assign 	 emif_s10_0_status_local_cal_fail_LED = ~ddr3_status_local_cal_fail ;
assign	 local_reset_done_LED = (ddr3_local_reset_done)? 1'b0 : 1'b1;
assign 	local_reset_not_done_LED = (!ddr3_local_reset_done) ? 1'b0 : 1'b1;

endmodule

# Ethernet and EMIF Platform

## Brief Overview
This project facilitates the passage and storage of data from a workstation to an external DDR3 memory device on a Stratix 10 GX FPGA board

## Hardware Description
This project uses Stratix 10 GX FPGA L-Tile (1SG280LU2F50E2VGS3) board. The external memory device used is a DDR3 x72 HiLo memory daughter card. 
The project uses the Intel Triple Speed Ethernet (TSE) IP to facilitate the Ethernet protocol. 
A NIOS II processor is also instantiated to facilitate the data transfer. 

![Hardware Platform](readme_fig/hardware_schematic.png)

## Software
The project includes a socket program (Server and Client) that is used to co-ordinate the data transfer. This is found in ``/software/``. 
The Socket server is instantiated on the FPGA using the NicheStack TCP/IP protocol. This will find an IP address and assign it to the board for the client to connect. 

### Design Flow
1.  A connection from the client is established to the  FPGA Server. Upon a successful connection, a Welcome Menu is sent from the server to the client. 

2. The client may choose the option "Acquire", which would prepare the board to acquire data. 

3. The client sends an int ``data_size`` which refers to the number of data floats being sent. The server mallocs the space required to receive ``data_size`` floats in the DDR3. 

4. The client generates and sends ``data_size`` number of 4-byte floats. The server receives them and stores them inside the DDR3

5. The server closes the connection after all the floats are received.

### BSP
There are 3 memory devices instantiated by the Hardware platform: On-chip memory, Flash Memory and SDRAM Memory. 

The mapping of memory in the linker script should be as follows:

| .text          | .rodata        | .rwdata        | .bss           | .heap      | .stack     |
|----------------|----------------|----------------|----------------|------------|------------|
| on_chip_ram_m9 | on_chip_ram_m9 | on_chip_ram_m9 | on_chip_ram_m9 | EMIF\_S10\_0 | EMIF_S10_0 |

The exception and reset vectors are also set to on\_chip\_ram\_m9.

### Running the program
The NIOS code can be excecuted via the Eclipse Interface by selecting Run As->Nios II Hardware. The BSP files should have been made prior to this step.

The client code ``\software\bulk_send_clinet`` must be compiled first: ``gcc bulk_send_clinet.c -o bulk_send``. 

It can then be excuted via ``./bulk_send [IP ADDRESS] [PORT NUMBER] [DATA_SIZE]``

## Known Issues
The SDRAM is the primary memory for the Ethernet instantiation. For this reason, there may be some memory overwriting between contents been initialized by the MAC and data been transferred to the FPGA from the workstation. A different hardware design is an alternative idea being worked on.

This project utilizes only half the span of the DDR3 due to memory address constraints.  A further version of this project will address this issue. 

## Acknowledgments
Much of the design to set up the Ethernet interface is based from the Intel Stratix 10 GX Board Update Portal example. 

___
This project is still a Work in Progress

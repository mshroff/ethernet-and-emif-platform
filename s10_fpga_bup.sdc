post_message -type info "Entering s10_fpga_bup.sdc"

set altera_reserved_tck { altera_reserved_tck }
create_clock -period 20.00 -name clk_50m_s10  [ get_ports clk_50m_s10  ]
create_clock -period  8.00 -name clk_s10top_125m_p  [ get_ports clk_s10top_125m_p  ]

create_clock -period "40.000 ns" -name {altera_reserved_tck} {altera_reserved_tck}

#JTAG Signal Constraints
#constrain the TDI TMS and TDO ports  -- (modified from timequest SDC cookbook)
set_input_delay  -clock altera_reserved_tck 5 [get_ports altera_reserved_tdi]
set_input_delay  -clock altera_reserved_tck 5 [get_ports altera_reserved_tms]
set_output_delay -clock altera_reserved_tck -clock_fall -fall -max 5 [get_ports altera_reserved_tdo]

#set_max_delay -to [get_ports {altera_reserved_tdo}] 1
#set_max_delay -from [get_ports {altera_reserved_tdi}] 1
#set_max_delay -from [get_ports {altera_reserved_tms}] 1

derive_pll_clocks
derive_clock_uncertainty
						
#set_clock_groups \
#    -exclusive \
#    -group [get_clocks clk_50m_s10] \
#    -group [get_clocks clk_s10top_125m_p]
#


#set_clock_groups -asynchronous -group {clk_s10top_125m_p *|ALTLVDS_RX_component|auto_generated|rx[0]|* *ALTLVDS_TX_component|auto_generated|pll_fclk~PLL_OUTPUT_COUNTER|divclk}

#set_false_path  -from [get_keepers *reset_counter*resetn_out] -to *
#set_false_path  -from * -to [get_keepers *bit_synchronizer*p1]


# cpu_resetn
set_input_delay   -clock [ get_clocks clk_50m_s10 ] 10  [ get_ports {cpu_resetn} ]
#set_false_path -from [get_ports cpu_resetn] -to *
#set_false_path -from [get_ports global_resetn] -to *

# Ethernet MDIO interface
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {enet_mdc} ]
set_input_delay   -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {enet_mdio} ]
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {enet_mdio} ]
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {enet_resetn} ]

# flash interface
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {fm_a[*]} ]
set_input_delay   -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {fm_d[*]} ]
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {fm_d[*]} ]
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {flash_cen[*]} ]
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {flash_oen} ]
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {flash_resetn} ]
set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {flash_wen} ]

set_input_delay   -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {user_pb[*]} ]
#set_output_delay  -clock [ get_clocks clk_50m_s10 ] 2   [ get_ports {user_led[*]} ]
set_false_path -from * -to [get_ports {user_led[*]}]

set_input_delay  -clock { clk_50m_s10 } 5   [get_ports {i2c_1v8_sda}]
set_input_delay  -clock { clk_50m_s10 } 5   [get_ports {i2c_1v8_scl}]
set_output_delay -clock { clk_50m_s10 } 2.5 [get_ports {i2c_1v8_sda}]
set_output_delay -clock { clk_50m_s10 } 2.5 [get_ports {i2c_1v8_scl}]

set_false_path -from [get_ports cpu_resetn] -to *
set_false_path -from * -to [get_ports {flash_resetn}]
set_false_path -from [get_ports altera_reserved_ntrst] -to *

if { ! [is_post_route] } {
#setup 
set_max_delay -from *tcm_data_out_reg* -to * 10
#hold
#set_min_delay -from *tcm_data_out_reg* -to * 2
} 


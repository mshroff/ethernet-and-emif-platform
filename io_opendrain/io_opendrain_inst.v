	io_opendrain u0 (
		.din    (_connected_to_din_),    //   input,  width = 1,    din.export
		.dout   (_connected_to_dout_),   //  output,  width = 1,   dout.export
		.oe     (_connected_to_oe_),     //   input,  width = 1,     oe.export
		.pad_io (_connected_to_pad_io_)  //   inout,  width = 1, pad_io.export
	);


`timescale 1ns / 100ps

module  reset_generator
        (
        input   wire  i_clk             , // input clock
        input   wire  i_reset_n         , // input reset, low active
        output  wire  o_reset_n_0       , // output reset #0, low active, de-asserted first
        output  wire  o_reset_n_1         // output reset #1, low active, de-asserted later
        );

//--------------------------------------------------
// Signal definition
//--------------------------------------------------
wire            clk;
wire            reset_n_in;
reg             reset_n_out0;
reg             reset_n_out1;

reg     [9:0]   ctr;
reg             slow_clk;
reg     [15:0]  reset_cnt;

assign  clk         = i_clk;
assign  reset_n_in  = i_reset_n;
assign  o_reset_n_0 = reset_n_out0;
assign  o_reset_n_1 = reset_n_out1;

//--------------------------------------------------
// Main function body
//--------------------------------------------------
always @(posedge clk or negedge reset_n_in)
begin
  if(!reset_n_in)
     begin
     ctr      <= 10'd0;
     slow_clk <= 1'b0;
     end
  else if(ctr == 10'd1023)
     begin
     ctr      <= 10'd0;
     slow_clk <= !slow_clk;
     end
  else if(ctr == 10'd511)
     begin
     ctr      <= ctr + 1'b1;
     slow_clk <= !slow_clk;
     end
  else
     ctr <= ctr + 1'b1;
end

always @(posedge slow_clk or negedge reset_n_in)
begin
  if(!reset_n_in)
     reset_cnt <= 16'd0;
  else if (reset_cnt == 16'd65535)
     reset_cnt <= reset_cnt;
  else
     reset_cnt <= reset_cnt + 1'b1;
end

always @(posedge slow_clk or negedge reset_n_in)
begin
  if(!reset_n_in)
     begin
     reset_n_out0 <= 1'b0;
     reset_n_out1 <= 1'b0;
     end
  else if (reset_cnt == 16'd32767)
     begin
     reset_n_out0 <= 1'b1;
     end
  else if (reset_cnt == 16'd65535)
     begin
     reset_n_out1 <= 1'b1;
     end
  else
     begin
     reset_n_out0 <= reset_n_out0;
     reset_n_out1 <= reset_n_out1;
     end
end

endmodule
//------------------------------------------------------------------------------
// end of file
//------------------------------------------------------------------------------
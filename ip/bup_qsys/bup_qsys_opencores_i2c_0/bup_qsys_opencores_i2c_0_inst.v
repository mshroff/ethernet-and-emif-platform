	bup_qsys_opencores_i2c_0 u0 (
		.wb_adr_i   (_connected_to_wb_adr_i_),   //   input,  width = 3,   avalon_slave_0.address
		.wb_dat_i   (_connected_to_wb_dat_i_),   //   input,  width = 8,                 .writedata
		.wb_dat_o   (_connected_to_wb_dat_o_),   //  output,  width = 8,                 .readdata
		.wb_we_i    (_connected_to_wb_we_i_),    //   input,  width = 1,                 .write
		.wb_stb_i   (_connected_to_wb_stb_i_),   //   input,  width = 1,                 .chipselect
		.wb_ack_o   (_connected_to_wb_ack_o_),   //  output,  width = 1,                 .waitrequest_n
		.wb_clk_i   (_connected_to_wb_clk_i_),   //   input,  width = 1,            clock.clk
		.wb_rst_i   (_connected_to_wb_rst_i_),   //   input,  width = 1,      clock_reset.reset
		.scl_pad_io (_connected_to_scl_pad_io_), //   inout,  width = 1,         export_0.scl_pad_io
		.sda_pad_io (_connected_to_sda_pad_io_), //   inout,  width = 1,                 .sda_pad_io
		.wb_inta_o  (_connected_to_wb_inta_o_)   //  output,  width = 1, interrupt_sender.irq
	);


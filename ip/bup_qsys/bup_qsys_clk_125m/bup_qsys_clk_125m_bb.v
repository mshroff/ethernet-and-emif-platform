
module bup_qsys_clk_125m (
	clk_out,
	in_clk,
	reset_n,
	reset_n_out);	

	output		clk_out;
	input		in_clk;
	input		reset_n;
	output		reset_n_out;
endmodule

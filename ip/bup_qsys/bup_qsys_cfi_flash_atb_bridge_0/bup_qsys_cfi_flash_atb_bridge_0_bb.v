module bup_qsys_cfi_flash_atb_bridge_0 (
		input  wire        clk,                      //   clk.clk
		output wire [26:0] tcm_address_out,          //   out.tcm_address_out
		output wire [0:0]  tcm_read_n_out,           //      .tcm_read_n_out
		output wire [0:0]  tcm_write_n_out,          //      .tcm_write_n_out
		inout  wire [15:0] tcm_data_out,             //      .tcm_data_out
		output wire [0:0]  tcm_chipselect_n_out,     //      .tcm_chipselect_n_out
		input  wire        reset,                    // reset.reset
		input  wire        request,                  //   tcs.request
		output wire        grant,                    //      .grant
		input  wire [26:0] tcs_tcm_address_out,      //      .address_out
		input  wire [0:0]  tcs_tcm_read_n_out,       //      .read_n_out
		input  wire [0:0]  tcs_tcm_write_n_out,      //      .write_n_out
		input  wire [15:0] tcs_tcm_data_out,         //      .data_out
		input  wire        tcs_tcm_data_outen,       //      .data_outen
		output wire [15:0] tcs_tcm_data_in,          //      .data_in
		input  wire [0:0]  tcs_tcm_chipselect_n_out  //      .chipselect_n_out
	);
endmodule


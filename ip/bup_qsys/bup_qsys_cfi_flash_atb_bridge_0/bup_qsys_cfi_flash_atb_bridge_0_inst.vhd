	component bup_qsys_cfi_flash_atb_bridge_0 is
		port (
			clk                      : in    std_logic                     := 'X';             -- clk
			tcm_address_out          : out   std_logic_vector(26 downto 0);                    -- tcm_address_out
			tcm_read_n_out           : out   std_logic_vector(0 downto 0);                     -- tcm_read_n_out
			tcm_write_n_out          : out   std_logic_vector(0 downto 0);                     -- tcm_write_n_out
			tcm_data_out             : inout std_logic_vector(15 downto 0) := (others => 'X'); -- tcm_data_out
			tcm_chipselect_n_out     : out   std_logic_vector(0 downto 0);                     -- tcm_chipselect_n_out
			reset                    : in    std_logic                     := 'X';             -- reset
			request                  : in    std_logic                     := 'X';             -- request
			grant                    : out   std_logic;                                        -- grant
			tcs_tcm_address_out      : in    std_logic_vector(26 downto 0) := (others => 'X'); -- address_out
			tcs_tcm_read_n_out       : in    std_logic_vector(0 downto 0)  := (others => 'X'); -- read_n_out
			tcs_tcm_write_n_out      : in    std_logic_vector(0 downto 0)  := (others => 'X'); -- write_n_out
			tcs_tcm_data_out         : in    std_logic_vector(15 downto 0) := (others => 'X'); -- data_out
			tcs_tcm_data_outen       : in    std_logic                     := 'X';             -- data_outen
			tcs_tcm_data_in          : out   std_logic_vector(15 downto 0);                    -- data_in
			tcs_tcm_chipselect_n_out : in    std_logic_vector(0 downto 0)  := (others => 'X')  -- chipselect_n_out
		);
	end component bup_qsys_cfi_flash_atb_bridge_0;

	u0 : component bup_qsys_cfi_flash_atb_bridge_0
		port map (
			clk                      => CONNECTED_TO_clk,                      --   clk.clk
			tcm_address_out          => CONNECTED_TO_tcm_address_out,          --   out.tcm_address_out
			tcm_read_n_out           => CONNECTED_TO_tcm_read_n_out,           --      .tcm_read_n_out
			tcm_write_n_out          => CONNECTED_TO_tcm_write_n_out,          --      .tcm_write_n_out
			tcm_data_out             => CONNECTED_TO_tcm_data_out,             --      .tcm_data_out
			tcm_chipselect_n_out     => CONNECTED_TO_tcm_chipselect_n_out,     --      .tcm_chipselect_n_out
			reset                    => CONNECTED_TO_reset,                    -- reset.reset
			request                  => CONNECTED_TO_request,                  --   tcs.request
			grant                    => CONNECTED_TO_grant,                    --      .grant
			tcs_tcm_address_out      => CONNECTED_TO_tcs_tcm_address_out,      --      .address_out
			tcs_tcm_read_n_out       => CONNECTED_TO_tcs_tcm_read_n_out,       --      .read_n_out
			tcs_tcm_write_n_out      => CONNECTED_TO_tcs_tcm_write_n_out,      --      .write_n_out
			tcs_tcm_data_out         => CONNECTED_TO_tcs_tcm_data_out,         --      .data_out
			tcs_tcm_data_outen       => CONNECTED_TO_tcs_tcm_data_outen,       --      .data_outen
			tcs_tcm_data_in          => CONNECTED_TO_tcs_tcm_data_in,          --      .data_in
			tcs_tcm_chipselect_n_out => CONNECTED_TO_tcs_tcm_chipselect_n_out  --      .chipselect_n_out
		);


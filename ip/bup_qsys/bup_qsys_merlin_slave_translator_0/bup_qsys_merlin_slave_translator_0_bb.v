
module bup_qsys_merlin_slave_translator_0 (
	av_address,
	av_write,
	av_read,
	av_readdata,
	av_writedata,
	av_waitrequest,
	uav_address,
	uav_burstcount,
	uav_read,
	uav_write,
	uav_waitrequest,
	uav_readdatavalid,
	uav_byteenable,
	uav_readdata,
	uav_writedata,
	uav_lock,
	uav_debugaccess,
	clk,
	reset);	

	output	[7:0]	av_address;
	output		av_write;
	output		av_read;
	input	[31:0]	av_readdata;
	output	[31:0]	av_writedata;
	input		av_waitrequest;
	input	[31:0]	uav_address;
	input	[3:0]	uav_burstcount;
	input		uav_read;
	input		uav_write;
	output		uav_waitrequest;
	output		uav_readdatavalid;
	input	[3:0]	uav_byteenable;
	output	[31:0]	uav_readdata;
	input	[31:0]	uav_writedata;
	input		uav_lock;
	input		uav_debugaccess;
	input		clk;
	input		reset;
endmodule

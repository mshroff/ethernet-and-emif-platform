
module bup_qsys_mm_bridge_0 (
	clk,
	m0_waitrequest,
	m0_readdata,
	m0_readdatavalid,
	m0_burstcount,
	m0_writedata,
	m0_address,
	m0_write,
	m0_read,
	m0_byteenable,
	m0_debugaccess,
	reset,
	s0_waitrequest,
	s0_readdata,
	s0_readdatavalid,
	s0_burstcount,
	s0_writedata,
	s0_address,
	s0_write,
	s0_read,
	s0_byteenable,
	s0_debugaccess);	

	input		clk;
	input		m0_waitrequest;
	input	[31:0]	m0_readdata;
	input		m0_readdatavalid;
	output	[0:0]	m0_burstcount;
	output	[31:0]	m0_writedata;
	output	[7:0]	m0_address;
	output		m0_write;
	output		m0_read;
	output	[3:0]	m0_byteenable;
	output		m0_debugaccess;
	input		reset;
	output		s0_waitrequest;
	output	[31:0]	s0_readdata;
	output		s0_readdatavalid;
	input	[0:0]	s0_burstcount;
	input	[31:0]	s0_writedata;
	input	[7:0]	s0_address;
	input		s0_write;
	input		s0_read;
	input	[3:0]	s0_byteenable;
	input		s0_debugaccess;
endmodule

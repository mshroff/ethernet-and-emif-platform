module bup_qsys_sysid (
		input  wire        clock,    //           clk.clk
		output wire [31:0] readdata, // control_slave.readdata
		input  wire        address,  //              .address
		input  wire        reset_n   //         reset.reset_n
	);
endmodule


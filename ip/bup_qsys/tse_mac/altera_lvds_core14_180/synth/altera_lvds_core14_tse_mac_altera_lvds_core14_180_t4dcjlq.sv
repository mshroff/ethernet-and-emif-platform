// (C) 2001-2018 Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files from any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License Subscription 
// Agreement, Intel FPGA IP License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Intel and sold by 
// Intel or its authorized distributors.  Please refer to the applicable 
// agreement for further details.


`timescale 1 ps / 1 ps


////////////////////////////////////////////////////////////////
////LVDS SERDES CORE                  //
////////////////////////////////////////////////////////////////
module altera_lvds_core14_tse_mac_altera_lvds_core14_180_t4dcjlq
#(
    parameter NUM_CHANNELS = 1,
    parameter J_FACTOR = 10,
    parameter EXTERNAL_PLL = "false",
    parameter USE_BITSLIP = "false",
    parameter TX_OUTCLOCK_NON_STD_PHASE_SHIFT = "false", 
    parameter SILICON_REV = "20nm5",
    
    parameter SERDES_DPA_MODE = "OFF_MODE",
    parameter ALIGN_TO_RISING_EDGE_ONLY = "false",
    parameter LOSE_LOCK_ON_ONE_CHANGE = "false",
    parameter RESET_FIFO_AT_FIRST_LOCK = "false",
    parameter ENABLE_CLOCK_PIN_MODE = "false",
    parameter LOOPBACK_MODE = 0,
    parameter NET_PPM_VARIATION = "0",
    parameter IS_NEGATIVE_PPM_DRIFT = "false",
    parameter REGISTER_PARALLEL_DATA = "false",
    parameter USE_FALLING_CLOCK_EDGE = "false", 
    parameter TX_OUTCLOCK_ENABLED = "false",
    parameter TX_OUTCLOCK_BYPASS_SERIALIZER = "false",
    parameter TX_OUTCLOCK_USE_FALLING_CLOCK_EDGE = "false", 
    parameter TX_REGISTER_CLOCK = "tx_coreclock", 
    parameter TX_OUTCLOCK_DIV_WORD = 0, 
    parameter VCO_DIV_EXPONENT = 0,
    parameter VCO_FREQUENCY = 0,
    parameter RX_BITSLIP_ROLLOVER = J_FACTOR,
    parameter WIDE_CT = "false",

    parameter USE_CPA="false",
    parameter CPA_DIVIDER=1,
    parameter CPA_CORE_DIVIDER="1.0",
    parameter USE_CPA_RECONFIG="false",
    
    
    parameter DPRIO_ENABLED = "false",
    parameter NUM_CLOCKS=1,
    parameter USE_DUPLEX="false"
    

) (
    input                                               ext_coreclock, 
    input                                               ext_fclk,
    input                                               ext_loaden,
    input                                               ext_tx_outclock_fclk,
    input                                               ext_tx_outclock_loaden,
    input       [7:0]                                   ext_vcoph,
    input                                               ext_pll_locked,
    input                                               inclock, 
    input       [NUM_CHANNELS-1:0]                      loopback_in,
    input                                               pll_areset,
    input       [NUM_CHANNELS-1:0]                      rx_bitslip_reset,
    input       [NUM_CHANNELS-1:0]                      rx_bitslip_ctrl,
    input       [NUM_CHANNELS-1:0]                      rx_dpa_reset,
    input       [NUM_CHANNELS-1:0]                      rx_dpa_hold,
    input       [NUM_CHANNELS-1:0]                      rx_fifo_reset,
    input       [NUM_CHANNELS-1:0]                      rx_in,
    input       [NUM_CHANNELS*J_FACTOR-1:0]             tx_in,

    input                                               user_dprio_rst_n,
    input                                               user_dprio_read,
    input       [8:0]                                   user_dprio_reg_addr,
    input                                               user_dprio_write,
    input       [7:0]                                   user_dprio_writedata,
    input                                               user_dprio_clk_in,
    input                                               user_mdio_dis,

    output                                              user_dprio_clk,
    output                                              user_dprio_block_select,
    output      [7:0]                                   user_dprio_readdata,
    output                                              user_dprio_ready,

   
    output      [NUM_CHANNELS-1:0]                      rx_bitslip_max, 
    output      [NUM_CHANNELS-1:0]                      loopback_out,
    output      [NUM_CHANNELS-1:0]                      rx_dpa_locked,
    output      [NUM_CHANNELS-1:0]                      rx_divfwdclk, 
    output      [NUM_CHANNELS*J_FACTOR-1:0]             rx_out,
    output                                              rx_coreclock,
    output                                              tx_coreclock, 
    output      [NUM_CHANNELS-1:0]                      tx_out,
    output                                              tx_outclock,
    output                                              pll_locked
);

    wire                                                clock_tree_loaden;
    wire                                                clock_tree_fclk;
    wire                                                tx_outclock_fclk;
    wire                                                pll_tx_outclock_fclk;
    wire                                                pll_tx_outclock_loaden;
    wire                                                fclk;    
    wire                                                loaden;    
    wire        [7:0]                                   vcoph;   
    wire                                                coreclock; 
    wire                                                dprio_clk; 
    wire                                                dprio_rst_n; 
    wire        [NUM_CHANNELS-1:0]                      rx_dpa_reset_internal;

    wire                                                pll_reset_coreclock;
    wire        [NUM_CHANNELS-1:0]                      rx_dpa_hold_wire;
    wire                                                pll_locked_only;
    wire                                                pll_core_reset;
    wire        [1:0]                                   cpa_loaden;
    wire                                                cpa_locked;
    wire        [1:0]                                   cpa_coreclk_out;
    wire                                                cpa_core_reset;
    wire                                                pa_dprio_clk;
    wire                                                pa_reset_n;
    wire                                                pll_dprio_clk;


    localparam SYNC_STAGES = 3;
    
    assign rx_coreclock  = coreclock; 
    assign tx_coreclock  = coreclock; 
   
    generate
            wire [8:0] pll_outclock;
            wire [1:0] pll_lvds_clk;
            wire [1:0] pll_loaden;
            
            if (WIDE_CT == "true")
            begin : wide_ct
                assign fclk   = pll_lvds_clk[1]; 
                assign loaden = pll_loaden[1];
               
                if (TX_OUTCLOCK_ENABLED == "true" && TX_OUTCLOCK_NON_STD_PHASE_SHIFT == "true")
                begin : tx_outclk
                    assign pll_tx_outclock_fclk   = pll_lvds_clk[0];
                    assign pll_tx_outclock_loaden = pll_loaden[0];
                end
            end
            else 
            begin : nonwide_ct
                assign fclk = pll_lvds_clk[0]; 
                if (SERDES_DPA_MODE != "dpa_mode_cdr")
                    assign loaden = pll_loaden[0];
            end

            if (USE_CPA != "true")
            begin : cpa_connect

                assign coreclock        = pll_outclock[4];
                assign pll_locked       = pll_locked_only;
                assign pll_core_reset   = pll_areset;
                
            end 
            else 
            begin : no_cpa_connect
                if (WIDE_CT == "true")
                begin : loaden_connect
                    assign cpa_loaden   = {1'b0, pll_loaden[0]};
                end
                else 
                begin : loaden_connect
                    assign cpa_loaden   = {1'b0,loaden};
                end 
            end 
               
            ////////////////////////////////////////////////////////////////
            ////PLL INSTANCE                       //
            ////////////////////////////////////////////////////////////////
            tse_mac_altera_lvds_core14_180_ihbi3bq pll_inst (
                .rst      (pll_areset),
                .refclk   (inclock),
                .locked   (pll_locked_only),
                .phout    (vcoph),
                .outclk_4 (pll_outclock[4]),
                .lvds_clk (pll_lvds_clk),
                .loaden   (pll_loaden)
            );



    if (USE_CPA == "true")
    begin : cpa_inst 
	    localparam [2:0] PA_EXPONENT = (CPA_DIVIDER == 1  ) ? 3'b000 :
	                                   (CPA_DIVIDER == 2  ) ? 3'b001 :
	                                   (CPA_DIVIDER == 4  ) ? 3'b010 : 
	                                   (CPA_DIVIDER == 8  ) ? 3'b011 : 
	                                   (CPA_DIVIDER == 16 ) ? 3'b100 : 
	                                   (CPA_DIVIDER == 32 ) ? 3'b101 : 
	                                   (CPA_DIVIDER == 64 ) ? 3'b110 : 
	                                   (CPA_DIVIDER == 128) ? 3'b111 : 
                                                              3'b000 ;
        reset_sync_w_lock reset_core (
               .pll_areset(pll_areset),
               .pll_locked(pll_locked),
               .clock(coreclock),
               .sync_reset(pll_core_reset)
        );

        reset_synchronizer_active_high #(
            .RESET_SYNC_LENGTH(3)
        ) reset_cpa_core (
            .async_reset  (pll_areset),
            .clock        (cpa_coreclk_out[0]),
            .sync_reset   (cpa_core_reset)
        );

        assign pll_locked= pll_locked_only & cpa_locked;

        if (CPA_CORE_DIVIDER == "1.5")
        begin : cpa_div
            div1p5 core_div (
                    .srst    (cpa_core_reset),
                    .inclk   (cpa_coreclk_out[0]),
                    .outclk  (coreclock)
            );
        end 
        else if (CPA_CORE_DIVIDER == "2.5")
        begin : cpa_div
            div2p5 core_div (
                    .srst    (cpa_core_reset),
                    .inclk   (cpa_coreclk_out[0]),
                    .outclk  (coreclock)
            );
        end 
        else if (CPA_CORE_DIVIDER == "3.5")
        begin : cpa_div
            div3p5 core_div (
                    .srst    (cpa_core_reset),
                    .inclk   (cpa_coreclk_out[0]),
                    .outclk  (coreclock)
            );
        end 
        else if (CPA_CORE_DIVIDER == "4.5")
        begin : cpa_div
            div4p5 core_div (
                    .srst    (cpa_core_reset),
                    .inclk   (cpa_coreclk_out[0]),
                    .outclk  (coreclock)
            );
        end
        else if (CPA_CORE_DIVIDER == "3.0")
        begin : cpa_div
            div3 core_div (
                    .srst    (cpa_core_reset),
                    .inclk   (cpa_coreclk_out[0]),
                    .outclk  (coreclock)
            );
        end 
        else if (CPA_CORE_DIVIDER == "5.0")
        begin : cpa_div
            div5 core_div (
                    .srst    (cpa_core_reset),
                    .inclk   (cpa_coreclk_out[0]),
                    .outclk  (coreclock)
            );
        end 
        else     
        begin : cpa_div
            assign coreclock= cpa_coreclk_out[0];
        end 
       
        if (USE_CPA_RECONFIG == "true")
        begin 
            assign user_dprio_ready = cpa_locked;    
            assign dprio_rst_n = user_dprio_rst_n;
            assign dprio_clk = pa_dprio_clk;

        end 
        else
        begin
            assign user_dprio_ready = 1'b0;
        end 
        
        assign pa_reset_n = ~pll_areset;

        ////////////////////////////////////////////////////////////////
        ////Clock Phase Alignment Block                //
        ////////////////////////////////////////////////////////////////
        //Instantiating the new ATOM based on Tile Ctrl
        fourteennm_cpa #(
	    	.pa_filter_code         (VCO_FREQUENCY       ),        
	    	.pa_sim_mode            ("long"              ),       
	    	.pa_phase_offset_0      (12'b0               ),       
	    	.pa_phase_offset_1      (12'b0               ),       
	    	.pa_exponent_0          (PA_EXPONENT         ),       
	    	.pa_exponent_1          (3'b0                ),       
	    	.pa_feedback_divider_c0 ("div_by_1"          ),       
	    	.pa_feedback_divider_c1 ("div_by_1"          ),       
	    	.pa_feedback_divider_p0 ("div_by_1"          ),       
	    	.pa_feedback_divider_p1 ("div_by_1"          ),       
	    	.pa_feedback_mux_sel_0  ("fb0_p_clk"         ),       
	    	.pa_feedback_mux_sel_1  ("fb0_p_clk"         ),       
	    	.pa_freq_track_speed    (4'hd                ),
	    	.pa_track_speed         (5'h18               ),                                         
	    	.pa_sync_control        ("no_sync"           ),
	    	.pa_sync_latency        (4'b0000             ),
	    	.physeq_core_clk_sel	("CLK0"		         ),
	    	.silicon_rev	        (SILICON_REV)
	    	) u_lvds_cpa (
	    	.global_reset_n    (~pll_areset               ),                                   
	    	
	    	.pll_locked_in     (pll_locked_only               ),    
	    	.pll_vco_in        (vcoph                    ),    
	    	.phy_clk_in        (cpa_loaden            ),    
	    	.phy_clk_out0      (),
	    	.phy_clk_out1      (),
	    	.phy_clk_out2      (),
	    	.phy_clk_out3      (),

	    	.dll_clk_in        (),
	    	.dll_clk_out0      (),
	    	.dll_clk_out1      (),
	    	.dll_clk_out2      (),
	    	.dll_clk_out3      (),
	    	
	    	.cal_avl_in        (),
	    	.cal_avl_rdata_out (),
	    	.cal_avl_out       (),
	    	.cal_avl_rdata_in  (),
	    	
	    	.pa_core_clk_in      ({1'b0,coreclock}        ),  
	    	.pa_core_clk_out     (cpa_coreclk_out         ),  
	    	.pa_locked           (cpa_locked),                         
	    	.pa_reset_n          (pa_reset_n            ),  
	    	.pa_core_in          (12'b000000000000       ),  
	    	.pa_fbclk_in         (1'b0                   ),  
	    	.pa_sync_data_bot_in (),                         
	    	.pa_sync_data_top_in (),                       
	    	.pa_sync_clk_bot_in  (),                       
	    	.pa_sync_clk_top_in  (),                       
	    	.pa_sync_data_bot_out(),                       
	    	.pa_sync_data_top_out(),                       
	    	.pa_sync_clk_bot_out (),                       
	    	.pa_sync_clk_top_out (),                       

	    	.dqs_in_x8_0       	(),
	    	.dqs_in_x8_1       	(),
	    	.dqs_in_x8_2       	(),
	    	.dqs_in_x8_3       	(),
	    	.dqs_in_x18_0      	(),
	    	.dqs_in_x18_1      	(),
	    	.dqs_in_x36        	(),
	    	.dqs_out_x8_lane0  	(),
	    	.dqs_out_x18_lane0 	(),
	    	.dqs_out_x36_lane0 	(),
	    	.dqs_out_x8_lane1  	(),
	    	.dqs_out_x18_lane1 	(),
	    	.dqs_out_x36_lane1 	(),
	    	.dqs_out_x8_lane2  	(),
	    	.dqs_out_x18_lane2 	(),
	    	.dqs_out_x36_lane2 	(),
	    	.dqs_out_x8_lane3  	(),
	    	.dqs_out_x18_lane3 	(),
	    	.dqs_out_x36_lane3 	(),

            .pa_mdio_dis           (user_mdio_dis          ),

            .pa_dprio_clk          (                       ), 
	    	.pa_dprio_rst_n        (                       ),
	    	.pa_dprio_read         (user_dprio_read        ),
	    	.pa_dprio_reg_addr     (user_dprio_reg_addr    ),
	    	.pa_dprio_write        (user_dprio_write       ),
	    	.pa_dprio_writedata    (user_dprio_writedata   ),
	    	.pa_dprio_block_select (user_dprio_block_select),
	    	.pa_dprio_readdata     (user_dprio_readdata)
	    );
    end

    endgenerate     

    
    

    generate 
        if  (ENABLE_CLOCK_PIN_MODE == "true" && ((SERDES_DPA_MODE == "tx_mode" || USE_DUPLEX == "true") || SERDES_DPA_MODE == "non_dpa_mode")) 
        begin : clock_pin_lvds_clock_tree
            fourteennm_lvds_clock_tree lvds_clock_tree_inst (
                .lvdsfclk_in(fclk),
                .lvdsfclk_out(clock_tree_fclk)
                );
        end
        else 
        begin : default_lvds_clock_tree 
                fourteennm_lvds_clock_tree lvds_clock_tree_inst (
                .lvdsfclk_in(fclk),
                .loaden_in(loaden),
                .lvdsfclk_out(clock_tree_fclk),
                .loaden_out(clock_tree_loaden)
                );
        end
    endgenerate 



    /////////////////////////////////////////////////
    ////SERDES INSTANCE               //
    /////////////////////////////////////////////////
    generate 
       if (SERDES_DPA_MODE == "non_dpa_mode" || SERDES_DPA_MODE == "dpa_mode_fifo" || SERDES_DPA_MODE == "dpa_mode_cdr" || SERDES_DPA_MODE == "tx_mode" && TX_REGISTER_CLOCK == "tx_coreclock")
       begin : reset_coreclk_sync 
           reset_synchronizer_active_high pll_areset_sync_coreclk (
               .async_reset(pll_core_reset),
               .clock(coreclock),
               .sync_reset(pll_reset_coreclock)
           );
       end
       else if (SERDES_DPA_MODE == "tx_mode" || USE_DUPLEX == "true")
       begin : reset_coreclk_sync 
           reset_synchronizer_active_high pll_areset_sync_coreclk (
               .async_reset(pll_core_reset),
               .clock(inclock),
               .sync_reset(pll_reset_coreclock)
           );
       end
    endgenerate

    genvar CH_INDEX;
    generate
        for (CH_INDEX=0;CH_INDEX<NUM_CHANNELS;CH_INDEX=CH_INDEX+1)
        begin : channels
            if (SERDES_DPA_MODE == "tx_mode" || USE_DUPLEX == "true")
            begin : tx                
                reg [J_FACTOR-1:0] tx_reg /* synthesis syn_preserve=1*/ ;
                wire [9:0] inv_tx_in = tx_in[(J_FACTOR*(CH_INDEX+1)-1):J_FACTOR*CH_INDEX];
                wire tx_core_reg_clk = (TX_REGISTER_CLOCK == "tx_coreclock")? coreclock : inclock;
                
                genvar i; 
                for (i=0; i<J_FACTOR; i=i+1)
                begin : input_reg
                    always @(posedge tx_core_reg_clk or posedge pll_reset_coreclock)
                    begin
                        if (pll_reset_coreclock == 1'b1)
                            tx_reg[i] <= 1'b0; 
                        else
                            tx_reg[i] <= inv_tx_in[J_FACTOR-1-i];
                    end     
                end
                (* altera_attribute = "-name MAX_WIRES_FOR_CORE_PERIPHERY_TRANSFER  2; -name MAX_WIRES_FOR_PERIPHERY_CORE_TRANSFER  1" *)
                fourteennm_io_serdes_dpa #(
                    .mode("tx_mode"),
                    .data_width(J_FACTOR),
                    .enable_clock_pin_mode(ENABLE_CLOCK_PIN_MODE),
                    .loopback_mode(LOOPBACK_MODE),
                    .vco_frequency(VCO_FREQUENCY),
                    .silicon_rev(SILICON_REV)
                ) serdes_dpa_inst (
                    .fclk(clock_tree_fclk),
                    .loaden(clock_tree_loaden),
                    .txdata(tx_reg),
                    .loopbackin(loopback_in[CH_INDEX]), 
                    .lvdsout(tx_out[CH_INDEX]),
                    .loopbackout(loopback_out[CH_INDEX])
                );
            end 
            if (SERDES_DPA_MODE == "non_dpa_mode")
            begin : rx_non_dpa
                wire [9:0] inv_rx_data;
                reg [J_FACTOR-1:0] rx_reg /* synthesis syn_preserve=1*/; 
                wire rx_bitslip_max_wire;

                (* altera_attribute = "-name MAX_WIRES_FOR_CORE_PERIPHERY_TRANSFER  2; -name MAX_WIRES_FOR_PERIPHERY_CORE_TRANSFER  1" *)
                fourteennm_io_serdes_dpa #(
                    .mode(SERDES_DPA_MODE),
                    .bitslip_rollover(RX_BITSLIP_ROLLOVER-1),
                    .data_width(J_FACTOR),
                    .enable_clock_pin_mode(ENABLE_CLOCK_PIN_MODE),
                    .loopback_mode(LOOPBACK_MODE),
                    .vco_frequency(VCO_FREQUENCY),
                    .silicon_rev(SILICON_REV)
                ) serdes_dpa_inst (
                    .bitslipcntl(rx_bitslip_ctrl[CH_INDEX]),
                    .bitslipreset(pll_core_reset|rx_bitslip_reset[CH_INDEX]),
                    .fclk(clock_tree_fclk),
                    .loaden(clock_tree_loaden),
                    .lvdsin(rx_in[CH_INDEX]),
                    .loopbackin(loopback_in[CH_INDEX]),
                    .bitslipmax(rx_bitslip_max_wire),
                    .rxdata(inv_rx_data), 
                    .loopbackout(loopback_out[CH_INDEX])
                );

                genvar i; 
                for (i=0; i<J_FACTOR; i=i+1)
                begin : output_reg 
                    always @(posedge coreclock or posedge pll_reset_coreclock)
                    begin
                        if (pll_reset_coreclock)
                            rx_reg[J_FACTOR-1-i] <= 1'b0; 
                        else 
                            rx_reg[J_FACTOR-1-i] <= inv_rx_data[J_FACTOR-1-i];
                    end
                end

                assign rx_out[(J_FACTOR*(CH_INDEX+1)-1):J_FACTOR*CH_INDEX] = rx_reg; 
                
                data_synchronizer #(.SYNC_LENGTH(SYNC_STAGES)) rx_bitslip_max_sync_inst (
                    .data_in(rx_bitslip_max_wire),
                    .clock(coreclock),
                    .reset(pll_reset_coreclock),
                    .data_out(rx_bitslip_max[CH_INDEX])
                ); 
           end
           else if (SERDES_DPA_MODE == "dpa_mode_fifo")
           begin : dpa_fifo
                wire [9:0] inv_rx_data;
                reg [J_FACTOR-1:0] rx_reg /* synthesis syn_preserve=1*/; 
                wire rx_bitslip_max_wire;
                wire rx_dpa_locked_wire;

                (* altera_attribute = "-name MAX_WIRES_FOR_CORE_PERIPHERY_TRANSFER  2; -name MAX_WIRES_FOR_PERIPHERY_CORE_TRANSFER  1" *)
                fourteennm_io_serdes_dpa #(
                    .mode(SERDES_DPA_MODE),
                    .align_to_rising_edge_only(ALIGN_TO_RISING_EDGE_ONLY),
                    .bitslip_rollover(RX_BITSLIP_ROLLOVER-1),
                    .data_width(J_FACTOR),
                    .lose_lock_on_one_change(LOSE_LOCK_ON_ONE_CHANGE),
                    .reset_fifo_at_first_lock(RESET_FIFO_AT_FIRST_LOCK),
                    .vco_div_exponent(VCO_DIV_EXPONENT),
                    .loopback_mode(LOOPBACK_MODE),
                    .vco_frequency(VCO_FREQUENCY),
                    .silicon_rev(SILICON_REV)
                ) serdes_dpa_inst (
                    .bitslipcntl(rx_bitslip_ctrl[CH_INDEX]),
                    .bitslipreset(pll_core_reset|rx_bitslip_reset[CH_INDEX]),
                    .dpahold(rx_dpa_hold[CH_INDEX]),
                    .dpareset(pll_core_reset|rx_dpa_reset_internal[CH_INDEX]),
                    .fclk(clock_tree_fclk),
                    .dpafiforeset(pll_core_reset|rx_fifo_reset[CH_INDEX]),
                    .loaden(clock_tree_loaden),
                    .lvdsin(rx_in[CH_INDEX]),
                    .dpaclk(vcoph),
                    .loopbackin(loopback_in[CH_INDEX]),
                    .bitslipmax(rx_bitslip_max_wire),
                    .dpalock(rx_dpa_locked_wire),
                    .rxdata(inv_rx_data),
                    .dprio_clk(dprio_clk),
                    .dprio_rst_n(dprio_rst_n),
                    .loopbackout(loopback_out[CH_INDEX])
                );

                genvar i; 
                for (i=0; i<J_FACTOR; i=i+1)
                begin : output_reg 
                    always @(posedge coreclock or posedge pll_reset_coreclock)
                    begin
                        if (pll_reset_coreclock)
                            rx_reg[J_FACTOR-1-i] <= 1'b0; 
                        else 
                            rx_reg[J_FACTOR-1-i] <= inv_rx_data[J_FACTOR-1-i];
                    end
                end

                assign rx_out[(J_FACTOR*(CH_INDEX+1)-1):J_FACTOR*CH_INDEX] = rx_reg; 
                
                data_synchronizer #(.SYNC_LENGTH(SYNC_STAGES)) rx_bitslip_max_sync_inst (
                    .data_in(rx_bitslip_max_wire),
                    .clock(coreclock),
                    .reset(pll_reset_coreclock),
                    .data_out(rx_bitslip_max[CH_INDEX])
                ); 

                data_synchronizer #(.SYNC_LENGTH(SYNC_STAGES)) rx_dpa_locked_sync_inst (
                    .data_in(rx_dpa_locked_wire),
                    .clock(coreclock),
                    .reset(pll_reset_coreclock),
                    .data_out(rx_dpa_locked[CH_INDEX])
                ); 
                
           end
           else if (SERDES_DPA_MODE == "dpa_mode_cdr")
           begin : soft_cdr
                wire [9:0] rx_data; 
                wire divfwdclk;
                reg [J_FACTOR-1:0] cdr_sync_reg /* synthesis syn_preserve=1*/;
                reg [J_FACTOR-1:0] rx_reg /* synthesis syn_preserve=1*/; 
                wire rx_bitslip_max_wire;
                wire rx_dpa_locked_wire;
                wire pll_areset_divfwdclk;
                wire pll_areset_rxdivfwdclk;

                (* altera_attribute = "-name MAX_WIRES_FOR_CORE_PERIPHERY_TRANSFER  2; -name MAX_WIRES_FOR_PERIPHERY_CORE_TRANSFER  1" *)
                fourteennm_io_serdes_dpa #(
                    .mode(SERDES_DPA_MODE),
                    .align_to_rising_edge_only(ALIGN_TO_RISING_EDGE_ONLY),
                    .bitslip_rollover(RX_BITSLIP_ROLLOVER-1),
                    .data_width(J_FACTOR),
                    .lose_lock_on_one_change(LOSE_LOCK_ON_ONE_CHANGE),
                    .reset_fifo_at_first_lock(RESET_FIFO_AT_FIRST_LOCK),
                    .enable_clock_pin_mode(ENABLE_CLOCK_PIN_MODE),
                    .loopback_mode(LOOPBACK_MODE),
                    .net_ppm_variation(NET_PPM_VARIATION),
                    .vco_div_exponent(VCO_DIV_EXPONENT),
                    .is_negative_ppm_drift(IS_NEGATIVE_PPM_DRIFT),
                    .vco_frequency(VCO_FREQUENCY),
                    .silicon_rev(SILICON_REV)
                ) serdes_dpa_inst (
                    .bitslipcntl(rx_bitslip_ctrl[CH_INDEX]),
                    .bitslipreset(pll_core_reset|rx_bitslip_reset[CH_INDEX]),
                    .dpahold(rx_dpa_hold[CH_INDEX]),
                    .dpareset(pll_core_reset|rx_dpa_reset_internal[CH_INDEX]),
                    .fclk(clock_tree_fclk), 
                    .lvdsin(rx_in[CH_INDEX]),
                    .dpaclk(vcoph),
                    .loopbackin(loopback_in[CH_INDEX]),
                    .bitslipmax(rx_bitslip_max_wire),
                    .dpalock(rx_dpa_locked_wire),
                    .rxdata(rx_data),
                    .pclk(divfwdclk), 
                    .dprio_clk(dprio_clk),
                    .dprio_rst_n(dprio_rst_n),
                    .loopbackout(loopback_out[CH_INDEX])
                );
                assign rx_divfwdclk[CH_INDEX] = ~divfwdclk;  

                reset_synchronizer_active_high pll_areset_sync_divfwdclk (
                    .async_reset(pll_core_reset),
                    .clock(divfwdclk),
                    .sync_reset(pll_areset_divfwdclk)
                );

                genvar i; 
                for (i=0; i<J_FACTOR; i=i+1)
                begin : cdr_sync
                    always @(posedge divfwdclk or posedge pll_areset_divfwdclk)
                    begin
                        if (pll_areset_divfwdclk)
                            cdr_sync_reg[i] <= 1'b0; 
                        else 
                            cdr_sync_reg[i] <= rx_data[i];
                    end
                end

                reset_synchronizer_active_high pll_areset_sync_rxdivfwdclk (
                    .async_reset(pll_areset),
                    .clock(rx_divfwdclk[CH_INDEX]),
                    .sync_reset(pll_areset_rxdivfwdclk)
                );

               
                always @(posedge rx_divfwdclk[CH_INDEX] or posedge pll_areset_rxdivfwdclk)
                begin 
                    if (pll_areset_rxdivfwdclk)
                        rx_reg <= {J_FACTOR{1'b0}}; 
                    else 
                        rx_reg <= cdr_sync_reg;
                end

                assign rx_out[(J_FACTOR*(CH_INDEX+1)-1):J_FACTOR*CH_INDEX] = rx_reg; 
                
                data_synchronizer #(.SYNC_LENGTH(SYNC_STAGES)) rx_bitslip_max_sync_inst (
                    .data_in(rx_bitslip_max_wire),
                    .clock(rx_divfwdclk[CH_INDEX]),
                    .reset(pll_areset_rxdivfwdclk),
                    .data_out(rx_bitslip_max[CH_INDEX])
                ); 

                data_synchronizer #(.SYNC_LENGTH(SYNC_STAGES)) rx_dpa_locked_sync_inst (
                    .data_in(rx_dpa_locked_wire),
                    .clock(rx_divfwdclk[CH_INDEX]),
                    .reset(pll_areset_rxdivfwdclk),
                    .data_out(rx_dpa_locked[CH_INDEX])
                ); 

            end    
        end
    endgenerate 

    
    generate 
        if ((SERDES_DPA_MODE == "tx_mode" || USE_DUPLEX == "true") && TX_OUTCLOCK_ENABLED == "true" && TX_OUTCLOCK_NON_STD_PHASE_SHIFT == "false") 
        begin : std_tx_outclock_serdes
        
            wire [9:0] tx_outclock_div_word = TX_OUTCLOCK_DIV_WORD;
            
            wire [9:0] tx_outclock_div_word_cell;
            genvar i; 
            for (i=0; i<J_FACTOR; i=i+1)
            begin : div_word_cells
                lcell div_word_wirelut (
                    .in(tx_outclock_div_word[i]),
                    .out(tx_outclock_div_word_cell[i])
                ) /* synthesis syn_keep = 1 */;
            end
            
            (* altera_attribute = "-name MAX_WIRES_FOR_CORE_PERIPHERY_TRANSFER  2; -name MAX_WIRES_FOR_PERIPHERY_CORE_TRANSFER  1" *)
            fourteennm_io_serdes_dpa #(
                .mode("tx_mode"),
                .data_width(J_FACTOR),
                .enable_clock_pin_mode(ENABLE_CLOCK_PIN_MODE),
                .bypass_serializer(TX_OUTCLOCK_BYPASS_SERIALIZER),
                .use_falling_clock_edge(TX_OUTCLOCK_USE_FALLING_CLOCK_EDGE), 
                .loopback_mode(LOOPBACK_MODE),
                .vco_frequency(VCO_FREQUENCY),
                .silicon_rev(SILICON_REV)
            ) serdes_dpa_tx_outclock (
                .fclk(clock_tree_fclk),
                .loaden(clock_tree_loaden),
                .txdata(tx_outclock_div_word_cell),
                .lvdsout(tx_outclock)
                );
        end
        else if ((SERDES_DPA_MODE == "tx_mode"|| USE_DUPLEX == "true") && TX_OUTCLOCK_ENABLED == "true" && TX_OUTCLOCK_NON_STD_PHASE_SHIFT == "true")
        begin : phase_shifted_tx_outclock_serdes 
            
            wire [9:0] tx_outclock_div_word = TX_OUTCLOCK_DIV_WORD;
            wire [9:0] tx_outclock_div_word_cell;
            genvar i; 
            for (i=0; i<J_FACTOR; i=i+1)
            begin : div_word_cells
                lcell div_word_wirelut (
                    .in(tx_outclock_div_word[i]),
                    .out(tx_outclock_div_word_cell[i])
                ) /* synthesis syn_keep = 1 */;
            end
            
            wire clock_tree_tx_outclock;
            wire clock_tree_tx_outclock_loaden;
            
            fourteennm_lvds_clock_tree outclock_tree (
                .lvdsfclk_in(pll_tx_outclock_fclk),
                .loaden_in(pll_tx_outclock_loaden),
                .lvdsfclk_out(clock_tree_tx_outclock),
                .loaden_out(clock_tree_tx_outclock_loaden)
                );

            (* altera_attribute = "-name MAX_WIRES_FOR_CORE_PERIPHERY_TRANSFER  2; -name MAX_WIRES_FOR_PERIPHERY_CORE_TRANSFER  1" *)
            fourteennm_io_serdes_dpa #(
                .mode("tx_mode"),
                .bypass_serializer(TX_OUTCLOCK_BYPASS_SERIALIZER),
                .loopback_mode(LOOPBACK_MODE),
                .vco_frequency(VCO_FREQUENCY),
                .is_tx_outclock("true"),
                .silicon_rev(SILICON_REV)
            ) serdes_dpa_tx_outclock (
                .fclk(clock_tree_tx_outclock),
                .loaden(clock_tree_tx_outclock_loaden),
                .txdata(tx_outclock_div_word_cell),
                .lvdsout(tx_outclock)
                );
        end
    endgenerate 
    
    generate
        if  (SERDES_DPA_MODE == "dpa_mode_cdr")
        begin : cdr_flop
            (* noprune *) reg cdr_dummy_flop; 
            always @(posedge coreclock or posedge pll_reset_coreclock)
            begin
                if (pll_reset_coreclock)
                    cdr_dummy_flop <= 1'b0;
                else 
                    cdr_dummy_flop <= 1'b1;
            end
        end
    endgenerate
    
    generate
        if (USE_CPA_RECONFIG == "true")
        begin : pa_dprio_clk_gen
            assign pa_dprio_clk = pll_dprio_clk;
        end 
        else
        begin : pa_dprio_clk_gen
            assign pa_dprio_clk = user_dprio_clk_in;            
        end 
        assign user_dprio_clk = pa_dprio_clk;
    endgenerate
    
    generate
        if  ((SERDES_DPA_MODE == "dpa_mode_fifo" || SERDES_DPA_MODE == "dpa_mode_cdr") && DPRIO_ENABLED == "true" )
        begin : dprio_clk_gen
            
            wire pll_reset_coreclock_dprio;
            lcell pll_reset_coreclock_wirelut (
                .in(pll_reset_coreclock),
                .out(pll_reset_coreclock_dprio)
            ) /* synthesis syn_keep = 1 */;
            
            reg dprio_done;
            reg dprio_start;
            
            
            wire dprio_clk_source = coreclock;
            reg [1:0] dprio_div_counter;
            wire dprio_gen_reset = ~pll_locked_only | dprio_done | pll_reset_coreclock_dprio;
            
            always @(posedge dprio_clk_source or posedge dprio_gen_reset)
            begin
                if (dprio_gen_reset)
                    dprio_div_counter <= 2'd0; 
                else
                    dprio_div_counter <= dprio_div_counter + 2'd1;
            end

            reg [7:0] dprio_cycle_counter;

            assign dprio_clk = dprio_div_counter[1];
            assign dprio_rst_n = ~dprio_gen_reset & dprio_start;
            
            always @(posedge dprio_clk or posedge dprio_gen_reset)
            begin
                if (dprio_gen_reset)
                    dprio_cycle_counter <= 7'd0; 
                else
                    dprio_cycle_counter <= dprio_cycle_counter + 7'd1;
            end

            assign user_dprio_ready = dprio_done;
            always @(posedge dprio_clk or posedge pll_reset_coreclock_dprio)
            begin
                if (pll_reset_coreclock_dprio)
                    dprio_start <= 1'b0;
                else if (&dprio_cycle_counter[1:0])
                    dprio_start <= 1'b1;
                    
                if (pll_reset_coreclock_dprio)
                    dprio_done <= 1'b0;
                else if (&dprio_cycle_counter)
                    dprio_done <= 1'b1;
            end
            
            assign rx_dpa_reset_internal = rx_dpa_reset | {NUM_CHANNELS{~dprio_done}};
        end
        else
        begin : dpa_reset
            assign rx_dpa_reset_internal = rx_dpa_reset;
        end
    endgenerate 

endmodule



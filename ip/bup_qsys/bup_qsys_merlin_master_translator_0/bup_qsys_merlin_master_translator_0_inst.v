	bup_qsys_merlin_master_translator_0 u0 (
		.av_address        (_connected_to_av_address_),        //      avalon_anti_master_0.address
		.av_waitrequest    (_connected_to_av_waitrequest_),    //                          .waitrequest
		.av_read           (_connected_to_av_read_),           //                          .read
		.av_readdata       (_connected_to_av_readdata_),       //                          .readdata
		.av_write          (_connected_to_av_write_),          //                          .write
		.av_writedata      (_connected_to_av_writedata_),      //                          .writedata
		.uav_address       (_connected_to_uav_address_),       // avalon_universal_master_0.address
		.uav_burstcount    (_connected_to_uav_burstcount_),    //                          .burstcount
		.uav_read          (_connected_to_uav_read_),          //                          .read
		.uav_write         (_connected_to_uav_write_),         //                          .write
		.uav_waitrequest   (_connected_to_uav_waitrequest_),   //                          .waitrequest
		.uav_readdatavalid (_connected_to_uav_readdatavalid_), //                          .readdatavalid
		.uav_byteenable    (_connected_to_uav_byteenable_),    //                          .byteenable
		.uav_readdata      (_connected_to_uav_readdata_),      //                          .readdata
		.uav_writedata     (_connected_to_uav_writedata_),     //                          .writedata
		.uav_lock          (_connected_to_uav_lock_),          //                          .lock
		.uav_debugaccess   (_connected_to_uav_debugaccess_),   //                          .debugaccess
		.clk               (_connected_to_clk_),               //                       clk.clk
		.reset             (_connected_to_reset_)              //                     reset.reset
	);


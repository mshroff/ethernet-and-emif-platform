	component bup_qsys_merlin_master_translator_0 is
		port (
			av_address        : in  std_logic_vector(7 downto 0)  := (others => 'X'); -- address
			av_waitrequest    : out std_logic;                                        -- waitrequest
			av_read           : in  std_logic                     := 'X';             -- read
			av_readdata       : out std_logic_vector(31 downto 0);                    -- readdata
			av_write          : in  std_logic                     := 'X';             -- write
			av_writedata      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			uav_address       : out std_logic_vector(37 downto 0);                    -- address
			uav_burstcount    : out std_logic_vector(9 downto 0);                     -- burstcount
			uav_read          : out std_logic;                                        -- read
			uav_write         : out std_logic;                                        -- write
			uav_waitrequest   : in  std_logic                     := 'X';             -- waitrequest
			uav_readdatavalid : in  std_logic                     := 'X';             -- readdatavalid
			uav_byteenable    : out std_logic_vector(3 downto 0);                     -- byteenable
			uav_readdata      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- readdata
			uav_writedata     : out std_logic_vector(31 downto 0);                    -- writedata
			uav_lock          : out std_logic;                                        -- lock
			uav_debugaccess   : out std_logic;                                        -- debugaccess
			clk               : in  std_logic                     := 'X';             -- clk
			reset             : in  std_logic                     := 'X'              -- reset
		);
	end component bup_qsys_merlin_master_translator_0;

	u0 : component bup_qsys_merlin_master_translator_0
		port map (
			av_address        => CONNECTED_TO_av_address,        --      avalon_anti_master_0.address
			av_waitrequest    => CONNECTED_TO_av_waitrequest,    --                          .waitrequest
			av_read           => CONNECTED_TO_av_read,           --                          .read
			av_readdata       => CONNECTED_TO_av_readdata,       --                          .readdata
			av_write          => CONNECTED_TO_av_write,          --                          .write
			av_writedata      => CONNECTED_TO_av_writedata,      --                          .writedata
			uav_address       => CONNECTED_TO_uav_address,       -- avalon_universal_master_0.address
			uav_burstcount    => CONNECTED_TO_uav_burstcount,    --                          .burstcount
			uav_read          => CONNECTED_TO_uav_read,          --                          .read
			uav_write         => CONNECTED_TO_uav_write,         --                          .write
			uav_waitrequest   => CONNECTED_TO_uav_waitrequest,   --                          .waitrequest
			uav_readdatavalid => CONNECTED_TO_uav_readdatavalid, --                          .readdatavalid
			uav_byteenable    => CONNECTED_TO_uav_byteenable,    --                          .byteenable
			uav_readdata      => CONNECTED_TO_uav_readdata,      --                          .readdata
			uav_writedata     => CONNECTED_TO_uav_writedata,     --                          .writedata
			uav_lock          => CONNECTED_TO_uav_lock,          --                          .lock
			uav_debugaccess   => CONNECTED_TO_uav_debugaccess,   --                          .debugaccess
			clk               => CONNECTED_TO_clk,               --                       clk.clk
			reset             => CONNECTED_TO_reset              --                     reset.reset
		);



module bup_qsys_merlin_master_translator_0 (
	av_address,
	av_waitrequest,
	av_read,
	av_readdata,
	av_write,
	av_writedata,
	uav_address,
	uav_burstcount,
	uav_read,
	uav_write,
	uav_waitrequest,
	uav_readdatavalid,
	uav_byteenable,
	uav_readdata,
	uav_writedata,
	uav_lock,
	uav_debugaccess,
	clk,
	reset);	

	input	[7:0]	av_address;
	output		av_waitrequest;
	input		av_read;
	output	[31:0]	av_readdata;
	input		av_write;
	input	[31:0]	av_writedata;
	output	[37:0]	uav_address;
	output	[9:0]	uav_burstcount;
	output		uav_read;
	output		uav_write;
	input		uav_waitrequest;
	input		uav_readdatavalid;
	output	[3:0]	uav_byteenable;
	input	[31:0]	uav_readdata;
	output	[31:0]	uav_writedata;
	output		uav_lock;
	output		uav_debugaccess;
	input		clk;
	input		reset;
endmodule

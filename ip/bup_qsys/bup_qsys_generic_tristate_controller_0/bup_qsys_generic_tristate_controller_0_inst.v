	bup_qsys_generic_tristate_controller_0 u0 (
		.clk_clk              (_connected_to_clk_clk_),              //   input,   width = 1,   clk.clk
		.reset_reset          (_connected_to_reset_reset_),          //   input,   width = 1, reset.reset
		.tcm_write_n_out      (_connected_to_tcm_write_n_out_),      //  output,   width = 1,   tcm.write_n_out
		.tcm_read_n_out       (_connected_to_tcm_read_n_out_),       //  output,   width = 1,      .read_n_out
		.tcm_chipselect_n_out (_connected_to_tcm_chipselect_n_out_), //  output,   width = 1,      .chipselect_n_out
		.tcm_request          (_connected_to_tcm_request_),          //  output,   width = 1,      .request
		.tcm_grant            (_connected_to_tcm_grant_),            //   input,   width = 1,      .grant
		.tcm_address_out      (_connected_to_tcm_address_out_),      //  output,  width = 27,      .address_out
		.tcm_data_out         (_connected_to_tcm_data_out_),         //  output,  width = 16,      .data_out
		.tcm_data_outen       (_connected_to_tcm_data_outen_),       //  output,   width = 1,      .data_outen
		.tcm_data_in          (_connected_to_tcm_data_in_),          //   input,  width = 16,      .data_in
		.uas_address          (_connected_to_uas_address_),          //   input,  width = 27,   uas.address
		.uas_burstcount       (_connected_to_uas_burstcount_),       //   input,   width = 2,      .burstcount
		.uas_read             (_connected_to_uas_read_),             //   input,   width = 1,      .read
		.uas_write            (_connected_to_uas_write_),            //   input,   width = 1,      .write
		.uas_waitrequest      (_connected_to_uas_waitrequest_),      //  output,   width = 1,      .waitrequest
		.uas_readdatavalid    (_connected_to_uas_readdatavalid_),    //  output,   width = 1,      .readdatavalid
		.uas_byteenable       (_connected_to_uas_byteenable_),       //   input,   width = 2,      .byteenable
		.uas_readdata         (_connected_to_uas_readdata_),         //  output,  width = 16,      .readdata
		.uas_writedata        (_connected_to_uas_writedata_),        //   input,  width = 16,      .writedata
		.uas_lock             (_connected_to_uas_lock_),             //   input,   width = 1,      .lock
		.uas_debugaccess      (_connected_to_uas_debugaccess_)       //   input,   width = 1,      .debugaccess
	);


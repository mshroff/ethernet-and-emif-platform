# Legal Notice: (C)2019 Altera Corporation. All rights reserved.  Your
# use of Altera Corporation's design tools, logic functions and other
# software and tools, and its AMPP partner logic functions, and any
# output files any of the foregoing (including device programming or
# simulation files), and any associated documentation or information are
# expressly subject to the terms and conditions of the Altera Program
# License Subscription Agreement or other applicable license agreement,
# including, without limitation, that your use is for the sole purpose
# of programming logic devices manufactured by Altera and sold by Altera
# or its authorized distributors.  Please refer to the applicable
# agreement for further details.

#**************************************************************
# Timequest JTAG clock definition
#   Uncommenting the following lines will define the JTAG
#   clock in TimeQuest Timing Analyzer
#**************************************************************

#create_clock -period 10MHz {altera_reserved_tck}
#set_clock_groups -asynchronous -group {altera_reserved_tck}

#**************************************************************
# Set TCL Path Variables 
#**************************************************************

set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva:*
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_oci:the_bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_oci
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_break 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_oci_break:the_bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_oci_break
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_ocimem 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_ocimem:the_bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_ocimem
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_debug 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_oci_debug:the_bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_nios2_oci_debug
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_wrapper 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_debug_slave_wrapper:the_bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_debug_slave_wrapper
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_tck 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_debug_slave_tck:the_bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_debug_slave_tck
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sysclk 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_debug_slave_sysclk:the_bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_debug_slave_sysclk
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_path 	 [format "%s|%s" $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci]
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_break_path 	 [format "%s|%s" $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_path $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_break]
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_ocimem_path 	 [format "%s|%s" $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_path $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_ocimem]
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_debug_path 	 [format "%s|%s" $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_path $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_debug]
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_tck_path 	 [format "%s|%s|%s" $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_path $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_wrapper $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_tck]
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sysclk_path 	 [format "%s|%s|%s" $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_path $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_wrapper $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sysclk]
set 	bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sr 	 [format "%s|*sr" $bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_tck_path]

#**************************************************************
# Set False Paths
#**************************************************************

set_false_path -from [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_break_path|break_readreg*] -to [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sr*]
set_false_path -from [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_debug_path|*resetlatch]     -to [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sr[33]]
set_false_path -from [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_debug_path|monitor_ready]  -to [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sr[0]]
set_false_path -from [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_debug_path|monitor_error]  -to [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sr[34]]
set_false_path -from [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_ocimem_path|*MonDReg*] -to [get_keepers *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sr*]
set_false_path -from *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sr*    -to *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sysclk_path|*jdo*
set_false_path -from *sld_jtag_hub:*|irf_reg* -to *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_jtag_sysclk_path|ir*
set_false_path -from *sld_jtag_hub:*|sld_shadow_jsm:shadow_jsm|state[1] -to *$bup_qsys_nios2_gen2_0_altera_nios2_gen2_unit_180_ojs2lva_oci_debug_path|monitor_go


module bup_qsys_high_res_timer (
	clk,
	irq,
	reset_n,
	address,
	writedata,
	readdata,
	chipselect,
	write_n);	

	input		clk;
	output		irq;
	input		reset_n;
	input	[2:0]	address;
	input	[15:0]	writedata;
	output	[15:0]	readdata;
	input		chipselect;
	input		write_n;
endmodule

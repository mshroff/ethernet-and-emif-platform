	component eth_tse is
		port (
			avalon_slave_address         : in  std_logic_vector(7 downto 0)  := (others => 'X'); -- address
			avalon_slave_write           : in  std_logic                     := 'X';             -- write
			avalon_slave_read            : in  std_logic                     := 'X';             -- read
			avalon_slave_writedata       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			avalon_slave_readdata        : out std_logic_vector(31 downto 0);                    -- readdata
			avalon_slave_waitrequest     : out std_logic;                                        -- waitrequest
			avalon_slave_eth_address     : out std_logic_vector(7 downto 0);                     -- address
			avalon_slave_eth_write       : out std_logic;                                        -- write
			avalon_slave_eth_read        : out std_logic;                                        -- read
			avalon_slave_eth_writedata   : out std_logic_vector(31 downto 0);                    -- writedata
			avalon_slave_eth_readdata    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- readdata
			avalon_slave_eth_waitrequest : in  std_logic                     := 'X';             -- waitrequest
			clock_sink_clk               : in  std_logic                     := 'X';             -- clk
			reset_source_reset           : in  std_logic                     := 'X';             -- reset
			reset_sink_eth_reset         : out std_logic                                         -- reset
		);
	end component eth_tse;

	u0 : component eth_tse
		port map (
			avalon_slave_address         => CONNECTED_TO_avalon_slave_address,         --  avalon_slave_eth.address
			avalon_slave_write           => CONNECTED_TO_avalon_slave_write,           --                  .write
			avalon_slave_read            => CONNECTED_TO_avalon_slave_read,            --                  .read
			avalon_slave_writedata       => CONNECTED_TO_avalon_slave_writedata,       --                  .writedata
			avalon_slave_readdata        => CONNECTED_TO_avalon_slave_readdata,        --                  .readdata
			avalon_slave_waitrequest     => CONNECTED_TO_avalon_slave_waitrequest,     --                  .waitrequest
			avalon_slave_eth_address     => CONNECTED_TO_avalon_slave_eth_address,     -- avalon_slave_eth1.address
			avalon_slave_eth_write       => CONNECTED_TO_avalon_slave_eth_write,       --                  .write
			avalon_slave_eth_read        => CONNECTED_TO_avalon_slave_eth_read,        --                  .read
			avalon_slave_eth_writedata   => CONNECTED_TO_avalon_slave_eth_writedata,   --                  .writedata
			avalon_slave_eth_readdata    => CONNECTED_TO_avalon_slave_eth_readdata,    --                  .readdata
			avalon_slave_eth_waitrequest => CONNECTED_TO_avalon_slave_eth_waitrequest, --                  .waitrequest
			clock_sink_clk               => CONNECTED_TO_clock_sink_clk,               --    clock_sink_eth.clk
			reset_source_reset           => CONNECTED_TO_reset_source_reset,           --    reset_sink_eth.reset
			reset_sink_eth_reset         => CONNECTED_TO_reset_sink_eth_reset          --   reset_sink_eth1.reset
		);



module eth_tse (
	avalon_slave_address,
	avalon_slave_write,
	avalon_slave_read,
	avalon_slave_writedata,
	avalon_slave_readdata,
	avalon_slave_waitrequest,
	avalon_slave_eth_address,
	avalon_slave_eth_write,
	avalon_slave_eth_read,
	avalon_slave_eth_writedata,
	avalon_slave_eth_readdata,
	avalon_slave_eth_waitrequest,
	clock_sink_clk,
	reset_source_reset,
	reset_sink_eth_reset);	

	input	[7:0]	avalon_slave_address;
	input		avalon_slave_write;
	input		avalon_slave_read;
	input	[31:0]	avalon_slave_writedata;
	output	[31:0]	avalon_slave_readdata;
	output		avalon_slave_waitrequest;
	output	[7:0]	avalon_slave_eth_address;
	output		avalon_slave_eth_write;
	output		avalon_slave_eth_read;
	output	[31:0]	avalon_slave_eth_writedata;
	input	[31:0]	avalon_slave_eth_readdata;
	input		avalon_slave_eth_waitrequest;
	input		clock_sink_clk;
	input		reset_source_reset;
	output		reset_sink_eth_reset;
endmodule

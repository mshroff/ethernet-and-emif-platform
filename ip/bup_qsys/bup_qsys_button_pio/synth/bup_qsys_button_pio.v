// bup_qsys_button_pio.v

// Generated using ACDS version 17.0ir3 182

`timescale 1 ps / 1 ps
module bup_qsys_button_pio (
		input  wire        clk,      //                 clk.clk
		input  wire [7:0]  in_port,  // external_connection.export
		input  wire        reset_n,  //               reset.reset_n
		input  wire [1:0]  address,  //                  s1.address
		output wire [31:0] readdata  //                    .readdata
	);

	bup_qsys_button_pio_altera_avalon_pio_16915_kmqchyi button_pio (
		.clk      (clk),      //                 clk.clk
		.reset_n  (reset_n),  //               reset.reset_n
		.address  (address),  //                  s1.address
		.readdata (readdata), //                    .readdata
		.in_port  (in_port)   // external_connection.export
	);

endmodule

	component bup_qsys_ext_flash is
		port (
			clk_clk              : in  std_logic                     := 'X';             -- clk
			reset_reset          : in  std_logic                     := 'X';             -- reset
			tcm_write_n_out      : out std_logic;                                        -- write_n_out
			tcm_read_n_out       : out std_logic;                                        -- read_n_out
			tcm_chipselect_n_out : out std_logic;                                        -- chipselect_n_out
			tcm_request          : out std_logic;                                        -- request
			tcm_grant            : in  std_logic                     := 'X';             -- grant
			tcm_address_out      : out std_logic_vector(27 downto 0);                    -- address_out
			tcm_data_out         : out std_logic_vector(31 downto 0);                    -- data_out
			tcm_data_outen       : out std_logic;                                        -- data_outen
			tcm_data_in          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- data_in
			uas_address          : in  std_logic_vector(27 downto 0) := (others => 'X'); -- address
			uas_burstcount       : in  std_logic_vector(2 downto 0)  := (others => 'X'); -- burstcount
			uas_read             : in  std_logic                     := 'X';             -- read
			uas_write            : in  std_logic                     := 'X';             -- write
			uas_waitrequest      : out std_logic;                                        -- waitrequest
			uas_readdatavalid    : out std_logic;                                        -- readdatavalid
			uas_byteenable       : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
			uas_readdata         : out std_logic_vector(31 downto 0);                    -- readdata
			uas_writedata        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			uas_lock             : in  std_logic                     := 'X';             -- lock
			uas_debugaccess      : in  std_logic                     := 'X'              -- debugaccess
		);
	end component bup_qsys_ext_flash;

	u0 : component bup_qsys_ext_flash
		port map (
			clk_clk              => CONNECTED_TO_clk_clk,              --   clk.clk
			reset_reset          => CONNECTED_TO_reset_reset,          -- reset.reset
			tcm_write_n_out      => CONNECTED_TO_tcm_write_n_out,      --   tcm.write_n_out
			tcm_read_n_out       => CONNECTED_TO_tcm_read_n_out,       --      .read_n_out
			tcm_chipselect_n_out => CONNECTED_TO_tcm_chipselect_n_out, --      .chipselect_n_out
			tcm_request          => CONNECTED_TO_tcm_request,          --      .request
			tcm_grant            => CONNECTED_TO_tcm_grant,            --      .grant
			tcm_address_out      => CONNECTED_TO_tcm_address_out,      --      .address_out
			tcm_data_out         => CONNECTED_TO_tcm_data_out,         --      .data_out
			tcm_data_outen       => CONNECTED_TO_tcm_data_outen,       --      .data_outen
			tcm_data_in          => CONNECTED_TO_tcm_data_in,          --      .data_in
			uas_address          => CONNECTED_TO_uas_address,          --   uas.address
			uas_burstcount       => CONNECTED_TO_uas_burstcount,       --      .burstcount
			uas_read             => CONNECTED_TO_uas_read,             --      .read
			uas_write            => CONNECTED_TO_uas_write,            --      .write
			uas_waitrequest      => CONNECTED_TO_uas_waitrequest,      --      .waitrequest
			uas_readdatavalid    => CONNECTED_TO_uas_readdatavalid,    --      .readdatavalid
			uas_byteenable       => CONNECTED_TO_uas_byteenable,       --      .byteenable
			uas_readdata         => CONNECTED_TO_uas_readdata,         --      .readdata
			uas_writedata        => CONNECTED_TO_uas_writedata,        --      .writedata
			uas_lock             => CONNECTED_TO_uas_lock,             --      .lock
			uas_debugaccess      => CONNECTED_TO_uas_debugaccess       --      .debugaccess
		);


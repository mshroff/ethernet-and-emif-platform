// bup_qsys_ext_flash.v

// Generated using ACDS version 17.0 233

`timescale 1 ps / 1 ps
module bup_qsys_ext_flash (
		input  wire        clk_clk,              //   clk.clk
		input  wire        reset_reset,          // reset.reset
		output wire        tcm_write_n_out,      //   tcm.write_n_out
		output wire        tcm_read_n_out,       //      .read_n_out
		output wire        tcm_chipselect_n_out, //      .chipselect_n_out
		output wire        tcm_request,          //      .request
		input  wire        tcm_grant,            //      .grant
		output wire [27:0] tcm_address_out,      //      .address_out
		output wire [31:0] tcm_data_out,         //      .data_out
		output wire        tcm_data_outen,       //      .data_outen
		input  wire [31:0] tcm_data_in,          //      .data_in
		input  wire [27:0] uas_address,          //   uas.address
		input  wire [2:0]  uas_burstcount,       //      .burstcount
		input  wire        uas_read,             //      .read
		input  wire        uas_write,            //      .write
		output wire        uas_waitrequest,      //      .waitrequest
		output wire        uas_readdatavalid,    //      .readdatavalid
		input  wire [3:0]  uas_byteenable,       //      .byteenable
		output wire [31:0] uas_readdata,         //      .readdata
		input  wire [31:0] uas_writedata,        //      .writedata
		input  wire        uas_lock,             //      .lock
		input  wire        uas_debugaccess       //      .debugaccess
	);

	bup_qsys_ext_flash_altera_generic_tristate_controller_170_z47i6dy #(
		.TCM_ADDRESS_W                  (28),
		.TCM_DATA_W                     (32),
		.TCM_BYTEENABLE_W               (4),
		.TCM_READ_WAIT                  (144),
		.TCM_WRITE_WAIT                 (144),
		.TCM_SETUP_WAIT                 (33),
		.TCM_DATA_HOLD                  (33),
		.TCM_TURNAROUND_TIME            (2),
		.TCM_TIMING_UNITS               (0),
		.TCM_READLATENCY                (2),
		.TCM_SYMBOLS_PER_WORD           (4),
		.USE_READDATA                   (1),
		.USE_WRITEDATA                  (1),
		.USE_READ                       (1),
		.USE_WRITE                      (1),
		.USE_BYTEENABLE                 (0),
		.USE_CHIPSELECT                 (1),
		.USE_LOCK                       (0),
		.USE_ADDRESS                    (1),
		.USE_WAITREQUEST                (0),
		.USE_WRITEBYTEENABLE            (0),
		.USE_OUTPUTENABLE               (0),
		.USE_RESETREQUEST               (0),
		.USE_IRQ                        (0),
		.USE_RESET_OUTPUT               (0),
		.ACTIVE_LOW_READ                (1),
		.ACTIVE_LOW_LOCK                (0),
		.ACTIVE_LOW_WRITE               (1),
		.ACTIVE_LOW_CHIPSELECT          (1),
		.ACTIVE_LOW_BYTEENABLE          (0),
		.ACTIVE_LOW_OUTPUTENABLE        (0),
		.ACTIVE_LOW_WRITEBYTEENABLE     (0),
		.ACTIVE_LOW_WAITREQUEST         (0),
		.ACTIVE_LOW_BEGINTRANSFER       (0),
		.CHIPSELECT_THROUGH_READLATENCY (0)
	) ext_flash (
		.clk_clk              (clk_clk),              //   clk.clk
		.reset_reset          (reset_reset),          // reset.reset
		.tcm_write_n_out      (tcm_write_n_out),      //   tcm.write_n_out
		.tcm_read_n_out       (tcm_read_n_out),       //      .read_n_out
		.tcm_chipselect_n_out (tcm_chipselect_n_out), //      .chipselect_n_out
		.tcm_request          (tcm_request),          //      .request
		.tcm_grant            (tcm_grant),            //      .grant
		.tcm_address_out      (tcm_address_out),      //      .address_out
		.tcm_data_out         (tcm_data_out),         //      .data_out
		.tcm_data_outen       (tcm_data_outen),       //      .data_outen
		.tcm_data_in          (tcm_data_in),          //      .data_in
		.uas_address          (uas_address),          //   uas.address
		.uas_burstcount       (uas_burstcount),       //      .burstcount
		.uas_read             (uas_read),             //      .read
		.uas_write            (uas_write),            //      .write
		.uas_waitrequest      (uas_waitrequest),      //      .waitrequest
		.uas_readdatavalid    (uas_readdatavalid),    //      .readdatavalid
		.uas_byteenable       (uas_byteenable),       //      .byteenable
		.uas_readdata         (uas_readdata),         //      .readdata
		.uas_writedata        (uas_writedata),        //      .writedata
		.uas_lock             (uas_lock),             //      .lock
		.uas_debugaccess      (uas_debugaccess)       //      .debugaccess
	);

endmodule

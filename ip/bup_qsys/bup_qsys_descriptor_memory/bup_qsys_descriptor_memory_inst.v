	bup_qsys_descriptor_memory u0 (
		.clk        (_connected_to_clk_),        //   clk1.clk
		.reset      (_connected_to_reset_),      // reset1.reset
		.reset_req  (_connected_to_reset_req_),  //       .reset_req
		.address    (_connected_to_address_),    //     s1.address
		.clken      (_connected_to_clken_),      //       .clken
		.chipselect (_connected_to_chipselect_), //       .chipselect
		.write      (_connected_to_write_),      //       .write
		.readdata   (_connected_to_readdata_),   //       .readdata
		.writedata  (_connected_to_writedata_),  //       .writedata
		.byteenable (_connected_to_byteenable_)  //       .byteenable
	);


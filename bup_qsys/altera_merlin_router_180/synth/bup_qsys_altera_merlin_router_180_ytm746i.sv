// (C) 2001-2018 Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files from any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License Subscription 
// Agreement, Intel FPGA IP License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Intel and sold by 
// Intel or its authorized distributors.  Please refer to the applicable 
// agreement for further details.



// Your use of Altera Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License Subscription 
// Agreement, Altera MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the applicable 
// agreement for further details.


// $Id: //acds/rel/18.0/ip/merlin/altera_merlin_router/altera_merlin_router.sv.terp#1 $
// $Revision: #1 $
// $Date: 2018/02/08 $
// $Author: psgswbuild $

// -------------------------------------------------------
// Merlin Router
//
// Asserts the appropriate one-hot encoded channel based on 
// either (a) the address or (b) the dest id. The DECODER_TYPE
// parameter controls this behaviour. 0 means address decoder,
// 1 means dest id decoder.
//
// In the case of (a), it also sets the destination id.
// -------------------------------------------------------

`timescale 1 ns / 1 ns

module bup_qsys_altera_merlin_router_180_ytm746i_default_decode
  #(
     parameter DEFAULT_CHANNEL = 6,
               DEFAULT_WR_CHANNEL = -1,
               DEFAULT_RD_CHANNEL = -1,
               DEFAULT_DESTID = 5 
   )
  (output [107 - 103 : 0] default_destination_id,
   output [21-1 : 0] default_wr_channel,
   output [21-1 : 0] default_rd_channel,
   output [21-1 : 0] default_src_channel
  );

  assign default_destination_id = 
    DEFAULT_DESTID[107 - 103 : 0];

  generate
    if (DEFAULT_CHANNEL == -1) begin : no_default_channel_assignment
      assign default_src_channel = '0;
    end
    else begin : default_channel_assignment
      assign default_src_channel = 21'b1 << DEFAULT_CHANNEL;
    end
  endgenerate

  generate
    if (DEFAULT_RD_CHANNEL == -1) begin : no_default_rw_channel_assignment
      assign default_wr_channel = '0;
      assign default_rd_channel = '0;
    end
    else begin : default_rw_channel_assignment
      assign default_wr_channel = 21'b1 << DEFAULT_WR_CHANNEL;
      assign default_rd_channel = 21'b1 << DEFAULT_RD_CHANNEL;
    end
  endgenerate

endmodule


module bup_qsys_altera_merlin_router_180_ytm746i
(
    // -------------------
    // Clock & Reset
    // -------------------
    input clk,
    input reset,

    // -------------------
    // Command Sink (Input)
    // -------------------
    input                       sink_valid,
    input  [130-1 : 0]    sink_data,
    input                       sink_startofpacket,
    input                       sink_endofpacket,
    output                      sink_ready,

    // -------------------
    // Command Source (Output)
    // -------------------
    output                          src_valid,
    output reg [130-1    : 0] src_data,
    output reg [21-1 : 0] src_channel,
    output                          src_startofpacket,
    output                          src_endofpacket,
    input                           src_ready
);

    // -------------------------------------------------------
    // Local parameters and variables
    // -------------------------------------------------------
    localparam PKT_ADDR_H = 67;
    localparam PKT_ADDR_L = 36;
    localparam PKT_DEST_ID_H = 107;
    localparam PKT_DEST_ID_L = 103;
    localparam PKT_PROTECTION_H = 111;
    localparam PKT_PROTECTION_L = 109;
    localparam ST_DATA_W = 130;
    localparam ST_CHANNEL_W = 21;
    localparam DECODER_TYPE = 0;

    localparam PKT_TRANS_WRITE = 70;
    localparam PKT_TRANS_READ  = 71;

    localparam PKT_ADDR_W = PKT_ADDR_H-PKT_ADDR_L + 1;
    localparam PKT_DEST_ID_W = PKT_DEST_ID_H-PKT_DEST_ID_L + 1;



    // -------------------------------------------------------
    // Figure out the number of bits to mask off for each slave span
    // during address decoding
    // -------------------------------------------------------
    localparam PAD0 = log2ceil(64'h40000000 - 64'h0); 
    localparam PAD1 = log2ceil(64'h50000000 - 64'h48000000); 
    localparam PAD2 = log2ceil(64'h50400000 - 64'h50200000); 
    localparam PAD3 = log2ceil(64'h50402000 - 64'h50400000); 
    localparam PAD4 = log2ceil(64'h50403000 - 64'h50402800); 
    localparam PAD5 = log2ceil(64'h50403400 - 64'h50403000); 
    localparam PAD6 = log2ceil(64'h50403420 - 64'h50403400); 
    localparam PAD7 = log2ceil(64'h50403440 - 64'h50403420); 
    localparam PAD8 = log2ceil(64'h50403460 - 64'h50403440); 
    localparam PAD9 = log2ceil(64'h50403480 - 64'h50403460); 
    localparam PAD10 = log2ceil(64'h504034a0 - 64'h50403480); 
    localparam PAD11 = log2ceil(64'h504034c0 - 64'h504034a0); 
    localparam PAD12 = log2ceil(64'h504034d0 - 64'h504034c0); 
    localparam PAD13 = log2ceil(64'h504034e0 - 64'h504034d0); 
    localparam PAD14 = log2ceil(64'h504034f0 - 64'h504034e0); 
    localparam PAD15 = log2ceil(64'h50403500 - 64'h504034f0); 
    localparam PAD16 = log2ceil(64'h50403510 - 64'h50403500); 
    localparam PAD17 = log2ceil(64'h50403520 - 64'h50403510); 
    localparam PAD18 = log2ceil(64'h50403530 - 64'h50403520); 
    localparam PAD19 = log2ceil(64'h50403538 - 64'h50403530); 
    localparam PAD20 = log2ceil(64'h50403540 - 64'h50403538); 
    // -------------------------------------------------------
    // Work out which address bits are significant based on the
    // address range of the slaves. If the required width is too
    // large or too small, we use the address field width instead.
    // -------------------------------------------------------
    localparam ADDR_RANGE = 64'h50403540;
    localparam RANGE_ADDR_WIDTH = log2ceil(ADDR_RANGE);
    localparam OPTIMIZED_ADDR_H = (RANGE_ADDR_WIDTH > PKT_ADDR_W) ||
                                  (RANGE_ADDR_WIDTH == 0) ?
                                        PKT_ADDR_H :
                                        PKT_ADDR_L + RANGE_ADDR_WIDTH - 1;

    localparam RG = RANGE_ADDR_WIDTH-1;
    localparam REAL_ADDRESS_RANGE = OPTIMIZED_ADDR_H - PKT_ADDR_L;

      reg [PKT_ADDR_W-1 : 0] address;
      always @* begin
        address = {PKT_ADDR_W{1'b0}};
        address [REAL_ADDRESS_RANGE:0] = sink_data[OPTIMIZED_ADDR_H : PKT_ADDR_L];
      end   

    // -------------------------------------------------------
    // Pass almost everything through, untouched
    // -------------------------------------------------------
    assign sink_ready        = src_ready;
    assign src_valid         = sink_valid;
    assign src_startofpacket = sink_startofpacket;
    assign src_endofpacket   = sink_endofpacket;
    wire [PKT_DEST_ID_W-1:0] default_destid;
    wire [21-1 : 0] default_src_channel;




    // -------------------------------------------------------
    // Write and read transaction signals
    // -------------------------------------------------------
    wire read_transaction;
    assign read_transaction  = sink_data[PKT_TRANS_READ];


    bup_qsys_altera_merlin_router_180_ytm746i_default_decode the_default_decode(
      .default_destination_id (default_destid),
      .default_wr_channel   (),
      .default_rd_channel   (),
      .default_src_channel  (default_src_channel)
    );

    always @* begin
        src_data    = sink_data;
        src_channel = default_src_channel;
        src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = default_destid;

        // --------------------------------------------------
        // Address Decoder
        // Sets the channel and destination ID based on the address
        // --------------------------------------------------

    // ( 0x0 .. 0x40000000 )
    if ( {address[RG:PAD0],{PAD0{1'b0}}} == 31'h0   ) begin
            src_channel = 21'b000000000000001000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 5;
    end

    // ( 0x48000000 .. 0x50000000 )
    if ( {address[RG:PAD1],{PAD1{1'b0}}} == 31'h48000000   ) begin
            src_channel = 21'b100000000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 6;
    end

    // ( 0x50200000 .. 0x50400000 )
    if ( {address[RG:PAD2],{PAD2{1'b0}}} == 31'h50200000   ) begin
            src_channel = 21'b000000000010000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 11;
    end

    // ( 0x50400000 .. 0x50402000 )
    if ( {address[RG:PAD3],{PAD3{1'b0}}} == 31'h50400000   ) begin
            src_channel = 21'b000000000100000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 4;
    end

    // ( 0x50402800 .. 0x50403000 )
    if ( {address[RG:PAD4],{PAD4{1'b0}}} == 31'h50402800   ) begin
            src_channel = 21'b000000000000010000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 3;
    end

    // ( 0x50403000 .. 0x50403400 )
    if ( {address[RG:PAD5],{PAD5{1'b0}}} == 31'h50403000   ) begin
            src_channel = 21'b000000000000000000100;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 20;
    end

    // ( 0x50403400 .. 0x50403420 )
    if ( {address[RG:PAD6],{PAD6{1'b0}}} == 31'h50403400   ) begin
            src_channel = 21'b000000010000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 14;
    end

    // ( 0x50403420 .. 0x50403440 )
    if ( {address[RG:PAD7],{PAD7{1'b0}}} == 31'h50403420   ) begin
            src_channel = 21'b000000000001000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 17;
    end

    // ( 0x50403440 .. 0x50403460 )
    if ( {address[RG:PAD8],{PAD8{1'b0}}} == 31'h50403440   ) begin
            src_channel = 21'b000000000000100000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 19;
    end

    // ( 0x50403460 .. 0x50403480 )
    if ( {address[RG:PAD9],{PAD9{1'b0}}} == 31'h50403460   ) begin
            src_channel = 21'b000000000000000100000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 18;
    end

    // ( 0x50403480 .. 0x504034a0 )
    if ( {address[RG:PAD10],{PAD10{1'b0}}} == 31'h50403480   ) begin
            src_channel = 21'b000000000000000010000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 16;
    end

    // ( 0x504034a0 .. 0x504034c0 )
    if ( {address[RG:PAD11],{PAD11{1'b0}}} == 31'h504034a0   ) begin
            src_channel = 21'b000000000000000000010;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 12;
    end

    // ( 0x504034c0 .. 0x504034d0 )
    if ( {address[RG:PAD12],{PAD12{1'b0}}} == 31'h504034c0   ) begin
            src_channel = 21'b010000000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 10;
    end

    // ( 0x504034d0 .. 0x504034e0 )
    if ( {address[RG:PAD13],{PAD13{1'b0}}} == 31'h504034d0  && read_transaction  ) begin
            src_channel = 21'b001000000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 1;
    end

    // ( 0x504034e0 .. 0x504034f0 )
    if ( {address[RG:PAD14],{PAD14{1'b0}}} == 31'h504034e0  && read_transaction  ) begin
            src_channel = 21'b000100000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 2;
    end

    // ( 0x504034f0 .. 0x50403500 )
    if ( {address[RG:PAD15],{PAD15{1'b0}}} == 31'h504034f0  && read_transaction  ) begin
            src_channel = 21'b000010000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 13;
    end

    // ( 0x50403500 .. 0x50403510 )
    if ( {address[RG:PAD16],{PAD16{1'b0}}} == 31'h50403500   ) begin
            src_channel = 21'b000001000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 7;
    end

    // ( 0x50403510 .. 0x50403520 )
    if ( {address[RG:PAD17],{PAD17{1'b0}}} == 31'h50403510  && read_transaction  ) begin
            src_channel = 21'b000000100000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 0;
    end

    // ( 0x50403520 .. 0x50403530 )
    if ( {address[RG:PAD18],{PAD18{1'b0}}} == 31'h50403520   ) begin
            src_channel = 21'b000000001000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 9;
    end

    // ( 0x50403530 .. 0x50403538 )
    if ( {address[RG:PAD19],{PAD19{1'b0}}} == 31'h50403530  && read_transaction  ) begin
            src_channel = 21'b000000000000000001000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 15;
    end

    // ( 0x50403538 .. 0x50403540 )
    if ( {address[RG:PAD20],{PAD20{1'b0}}} == 31'h50403538   ) begin
            src_channel = 21'b000000000000000000001;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 8;
    end

end


    // --------------------------------------------------
    // Ceil(log2()) function
    // --------------------------------------------------
    function integer log2ceil;
        input reg[65:0] val;
        reg [65:0] i;

        begin
            i = 1;
            log2ceil = 0;

            while (i < val) begin
                log2ceil = log2ceil + 1;
                i = i << 1;
            end
        end
    endfunction

endmodule



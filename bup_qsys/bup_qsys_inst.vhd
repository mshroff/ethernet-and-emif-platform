	component bup_qsys is
		port (
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_address_out      : out   std_logic_vector(26 downto 0);                    -- tcm_address_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_read_n_out       : out   std_logic_vector(0 downto 0);                     -- tcm_read_n_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_write_n_out      : out   std_logic_vector(0 downto 0);                     -- tcm_write_n_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_data_out         : inout std_logic_vector(15 downto 0) := (others => 'X'); -- tcm_data_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_chipselect_n_out : out   std_logic_vector(0 downto 0);                     -- tcm_chipselect_n_out
			button_pio_external_connection_export                    : in    std_logic_vector(2 downto 0)  := (others => 'X'); -- export
			cal_fail_external_connection_export                      : in    std_logic                     := 'X';             -- export
			cal_success_external_connection_export                   : in    std_logic                     := 'X';             -- export
			clk_125m_clk                                             : in    std_logic                     := 'X';             -- clk
			clk_133_clk                                              : in    std_logic                     := 'X';             -- clk
			emif_s10_0_local_reset_req_local_reset_req               : in    std_logic                     := 'X';             -- local_reset_req
			emif_s10_0_local_reset_status_local_reset_done           : out   std_logic;                                        -- local_reset_done
			emif_s10_0_mem_mem_ck                                    : out   std_logic_vector(0 downto 0);                     -- mem_ck
			emif_s10_0_mem_mem_ck_n                                  : out   std_logic_vector(0 downto 0);                     -- mem_ck_n
			emif_s10_0_mem_mem_a                                     : out   std_logic_vector(14 downto 0);                    -- mem_a
			emif_s10_0_mem_mem_ba                                    : out   std_logic_vector(2 downto 0);                     -- mem_ba
			emif_s10_0_mem_mem_cke                                   : out   std_logic_vector(0 downto 0);                     -- mem_cke
			emif_s10_0_mem_mem_cs_n                                  : out   std_logic_vector(0 downto 0);                     -- mem_cs_n
			emif_s10_0_mem_mem_odt                                   : out   std_logic_vector(0 downto 0);                     -- mem_odt
			emif_s10_0_mem_mem_reset_n                               : out   std_logic_vector(0 downto 0);                     -- mem_reset_n
			emif_s10_0_mem_mem_we_n                                  : out   std_logic_vector(0 downto 0);                     -- mem_we_n
			emif_s10_0_mem_mem_ras_n                                 : out   std_logic_vector(0 downto 0);                     -- mem_ras_n
			emif_s10_0_mem_mem_cas_n                                 : out   std_logic_vector(0 downto 0);                     -- mem_cas_n
			emif_s10_0_mem_mem_dqs                                   : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
			emif_s10_0_mem_mem_dqs_n                                 : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
			emif_s10_0_mem_mem_dq                                    : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
			emif_s10_0_mem_mem_dm                                    : out   std_logic_vector(3 downto 0);                     -- mem_dm
			emif_s10_0_oct_oct_rzqin                                 : in    std_logic                     := 'X';             -- oct_rzqin
			emif_s10_0_status_local_cal_success                      : out   std_logic;                                        -- local_cal_success
			emif_s10_0_status_local_cal_fail                         : out   std_logic;                                        -- local_cal_fail
			flash_select_external_connection_export                  : out   std_logic;                                        -- export
			led_pio_out_export                                       : out   std_logic_vector(7 downto 0);                     -- export
			local_reset_req_external_connection_export               : out   std_logic;                                        -- export
			opencores_i2c_export_0_scl_pad_io                        : inout std_logic                     := 'X';             -- scl_pad_io
			opencores_i2c_export_0_sda_pad_io                        : inout std_logic                     := 'X';             -- sda_pad_io
			reset_1_reset_n                                          : in    std_logic                     := 'X';             -- reset_n
			reset_125m_reset_n                                       : in    std_logic                     := 'X';             -- reset_n
			reset_done_external_connection_export                    : in    std_logic                     := 'X';             -- export
			sys_clk_clk                                              : in    std_logic                     := 'X';             -- clk
			tse_mac_mac_mdio_connection_mdc                          : out   std_logic;                                        -- mdc
			tse_mac_mac_mdio_connection_mdio_in                      : in    std_logic                     := 'X';             -- mdio_in
			tse_mac_mac_mdio_connection_mdio_out                     : out   std_logic;                                        -- mdio_out
			tse_mac_mac_mdio_connection_mdio_oen                     : out   std_logic;                                        -- mdio_oen
			tse_mac_misc_connection_magic_wakeup                     : out   std_logic;                                        -- magic_wakeup
			tse_mac_misc_connection_magic_sleep_n                    : in    std_logic                     := 'X';             -- magic_sleep_n
			tse_mac_misc_connection_ff_tx_crc_fwd                    : in    std_logic                     := 'X';             -- ff_tx_crc_fwd
			tse_mac_misc_connection_ff_tx_septy                      : out   std_logic;                                        -- ff_tx_septy
			tse_mac_misc_connection_tx_ff_uflow                      : out   std_logic;                                        -- tx_ff_uflow
			tse_mac_misc_connection_ff_tx_a_full                     : out   std_logic;                                        -- ff_tx_a_full
			tse_mac_misc_connection_ff_tx_a_empty                    : out   std_logic;                                        -- ff_tx_a_empty
			tse_mac_misc_connection_rx_err_stat                      : out   std_logic_vector(17 downto 0);                    -- rx_err_stat
			tse_mac_misc_connection_rx_frm_type                      : out   std_logic_vector(3 downto 0);                     -- rx_frm_type
			tse_mac_misc_connection_ff_rx_dsav                       : out   std_logic;                                        -- ff_rx_dsav
			tse_mac_misc_connection_ff_rx_a_full                     : out   std_logic;                                        -- ff_rx_a_full
			tse_mac_misc_connection_ff_rx_a_empty                    : out   std_logic;                                        -- ff_rx_a_empty
			tse_mac_serial_connection_rxp_0                          : in    std_logic                     := 'X';             -- rxp_0
			tse_mac_serial_connection_txp_0                          : out   std_logic;                                        -- txp_0
			tse_mac_status_led_connection_crs                        : out   std_logic;                                        -- crs
			tse_mac_status_led_connection_link                       : out   std_logic;                                        -- link
			tse_mac_status_led_connection_panel_link                 : out   std_logic;                                        -- panel_link
			tse_mac_status_led_connection_col                        : out   std_logic;                                        -- col
			tse_mac_status_led_connection_an                         : out   std_logic;                                        -- an
			tse_mac_status_led_connection_char_err                   : out   std_logic;                                        -- char_err
			tse_mac_status_led_connection_disp_err                   : out   std_logic                                         -- disp_err
		);
	end component bup_qsys;

	u0 : component bup_qsys
		port map (
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_address_out      => CONNECTED_TO_bup_qsys_cfi_flash_atb_bridge_0_out_tcm_address_out,      -- bup_qsys_cfi_flash_atb_bridge_0_out.tcm_address_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_read_n_out       => CONNECTED_TO_bup_qsys_cfi_flash_atb_bridge_0_out_tcm_read_n_out,       --                                    .tcm_read_n_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_write_n_out      => CONNECTED_TO_bup_qsys_cfi_flash_atb_bridge_0_out_tcm_write_n_out,      --                                    .tcm_write_n_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_data_out         => CONNECTED_TO_bup_qsys_cfi_flash_atb_bridge_0_out_tcm_data_out,         --                                    .tcm_data_out
			bup_qsys_cfi_flash_atb_bridge_0_out_tcm_chipselect_n_out => CONNECTED_TO_bup_qsys_cfi_flash_atb_bridge_0_out_tcm_chipselect_n_out, --                                    .tcm_chipselect_n_out
			button_pio_external_connection_export                    => CONNECTED_TO_button_pio_external_connection_export,                    --      button_pio_external_connection.export
			cal_fail_external_connection_export                      => CONNECTED_TO_cal_fail_external_connection_export,                      --        cal_fail_external_connection.export
			cal_success_external_connection_export                   => CONNECTED_TO_cal_success_external_connection_export,                   --     cal_success_external_connection.export
			clk_125m_clk                                             => CONNECTED_TO_clk_125m_clk,                                             --                            clk_125m.clk
			clk_133_clk                                              => CONNECTED_TO_clk_133_clk,                                              --                             clk_133.clk
			emif_s10_0_local_reset_req_local_reset_req               => CONNECTED_TO_emif_s10_0_local_reset_req_local_reset_req,               --          emif_s10_0_local_reset_req.local_reset_req
			emif_s10_0_local_reset_status_local_reset_done           => CONNECTED_TO_emif_s10_0_local_reset_status_local_reset_done,           --       emif_s10_0_local_reset_status.local_reset_done
			emif_s10_0_mem_mem_ck                                    => CONNECTED_TO_emif_s10_0_mem_mem_ck,                                    --                      emif_s10_0_mem.mem_ck
			emif_s10_0_mem_mem_ck_n                                  => CONNECTED_TO_emif_s10_0_mem_mem_ck_n,                                  --                                    .mem_ck_n
			emif_s10_0_mem_mem_a                                     => CONNECTED_TO_emif_s10_0_mem_mem_a,                                     --                                    .mem_a
			emif_s10_0_mem_mem_ba                                    => CONNECTED_TO_emif_s10_0_mem_mem_ba,                                    --                                    .mem_ba
			emif_s10_0_mem_mem_cke                                   => CONNECTED_TO_emif_s10_0_mem_mem_cke,                                   --                                    .mem_cke
			emif_s10_0_mem_mem_cs_n                                  => CONNECTED_TO_emif_s10_0_mem_mem_cs_n,                                  --                                    .mem_cs_n
			emif_s10_0_mem_mem_odt                                   => CONNECTED_TO_emif_s10_0_mem_mem_odt,                                   --                                    .mem_odt
			emif_s10_0_mem_mem_reset_n                               => CONNECTED_TO_emif_s10_0_mem_mem_reset_n,                               --                                    .mem_reset_n
			emif_s10_0_mem_mem_we_n                                  => CONNECTED_TO_emif_s10_0_mem_mem_we_n,                                  --                                    .mem_we_n
			emif_s10_0_mem_mem_ras_n                                 => CONNECTED_TO_emif_s10_0_mem_mem_ras_n,                                 --                                    .mem_ras_n
			emif_s10_0_mem_mem_cas_n                                 => CONNECTED_TO_emif_s10_0_mem_mem_cas_n,                                 --                                    .mem_cas_n
			emif_s10_0_mem_mem_dqs                                   => CONNECTED_TO_emif_s10_0_mem_mem_dqs,                                   --                                    .mem_dqs
			emif_s10_0_mem_mem_dqs_n                                 => CONNECTED_TO_emif_s10_0_mem_mem_dqs_n,                                 --                                    .mem_dqs_n
			emif_s10_0_mem_mem_dq                                    => CONNECTED_TO_emif_s10_0_mem_mem_dq,                                    --                                    .mem_dq
			emif_s10_0_mem_mem_dm                                    => CONNECTED_TO_emif_s10_0_mem_mem_dm,                                    --                                    .mem_dm
			emif_s10_0_oct_oct_rzqin                                 => CONNECTED_TO_emif_s10_0_oct_oct_rzqin,                                 --                      emif_s10_0_oct.oct_rzqin
			emif_s10_0_status_local_cal_success                      => CONNECTED_TO_emif_s10_0_status_local_cal_success,                      --                   emif_s10_0_status.local_cal_success
			emif_s10_0_status_local_cal_fail                         => CONNECTED_TO_emif_s10_0_status_local_cal_fail,                         --                                    .local_cal_fail
			flash_select_external_connection_export                  => CONNECTED_TO_flash_select_external_connection_export,                  --    flash_select_external_connection.export
			led_pio_out_export                                       => CONNECTED_TO_led_pio_out_export,                                       --                         led_pio_out.export
			local_reset_req_external_connection_export               => CONNECTED_TO_local_reset_req_external_connection_export,               -- local_reset_req_external_connection.export
			opencores_i2c_export_0_scl_pad_io                        => CONNECTED_TO_opencores_i2c_export_0_scl_pad_io,                        --              opencores_i2c_export_0.scl_pad_io
			opencores_i2c_export_0_sda_pad_io                        => CONNECTED_TO_opencores_i2c_export_0_sda_pad_io,                        --                                    .sda_pad_io
			reset_1_reset_n                                          => CONNECTED_TO_reset_1_reset_n,                                          --                             reset_1.reset_n
			reset_125m_reset_n                                       => CONNECTED_TO_reset_125m_reset_n,                                       --                          reset_125m.reset_n
			reset_done_external_connection_export                    => CONNECTED_TO_reset_done_external_connection_export,                    --      reset_done_external_connection.export
			sys_clk_clk                                              => CONNECTED_TO_sys_clk_clk,                                              --                             sys_clk.clk
			tse_mac_mac_mdio_connection_mdc                          => CONNECTED_TO_tse_mac_mac_mdio_connection_mdc,                          --         tse_mac_mac_mdio_connection.mdc
			tse_mac_mac_mdio_connection_mdio_in                      => CONNECTED_TO_tse_mac_mac_mdio_connection_mdio_in,                      --                                    .mdio_in
			tse_mac_mac_mdio_connection_mdio_out                     => CONNECTED_TO_tse_mac_mac_mdio_connection_mdio_out,                     --                                    .mdio_out
			tse_mac_mac_mdio_connection_mdio_oen                     => CONNECTED_TO_tse_mac_mac_mdio_connection_mdio_oen,                     --                                    .mdio_oen
			tse_mac_misc_connection_magic_wakeup                     => CONNECTED_TO_tse_mac_misc_connection_magic_wakeup,                     --             tse_mac_misc_connection.magic_wakeup
			tse_mac_misc_connection_magic_sleep_n                    => CONNECTED_TO_tse_mac_misc_connection_magic_sleep_n,                    --                                    .magic_sleep_n
			tse_mac_misc_connection_ff_tx_crc_fwd                    => CONNECTED_TO_tse_mac_misc_connection_ff_tx_crc_fwd,                    --                                    .ff_tx_crc_fwd
			tse_mac_misc_connection_ff_tx_septy                      => CONNECTED_TO_tse_mac_misc_connection_ff_tx_septy,                      --                                    .ff_tx_septy
			tse_mac_misc_connection_tx_ff_uflow                      => CONNECTED_TO_tse_mac_misc_connection_tx_ff_uflow,                      --                                    .tx_ff_uflow
			tse_mac_misc_connection_ff_tx_a_full                     => CONNECTED_TO_tse_mac_misc_connection_ff_tx_a_full,                     --                                    .ff_tx_a_full
			tse_mac_misc_connection_ff_tx_a_empty                    => CONNECTED_TO_tse_mac_misc_connection_ff_tx_a_empty,                    --                                    .ff_tx_a_empty
			tse_mac_misc_connection_rx_err_stat                      => CONNECTED_TO_tse_mac_misc_connection_rx_err_stat,                      --                                    .rx_err_stat
			tse_mac_misc_connection_rx_frm_type                      => CONNECTED_TO_tse_mac_misc_connection_rx_frm_type,                      --                                    .rx_frm_type
			tse_mac_misc_connection_ff_rx_dsav                       => CONNECTED_TO_tse_mac_misc_connection_ff_rx_dsav,                       --                                    .ff_rx_dsav
			tse_mac_misc_connection_ff_rx_a_full                     => CONNECTED_TO_tse_mac_misc_connection_ff_rx_a_full,                     --                                    .ff_rx_a_full
			tse_mac_misc_connection_ff_rx_a_empty                    => CONNECTED_TO_tse_mac_misc_connection_ff_rx_a_empty,                    --                                    .ff_rx_a_empty
			tse_mac_serial_connection_rxp_0                          => CONNECTED_TO_tse_mac_serial_connection_rxp_0,                          --           tse_mac_serial_connection.rxp_0
			tse_mac_serial_connection_txp_0                          => CONNECTED_TO_tse_mac_serial_connection_txp_0,                          --                                    .txp_0
			tse_mac_status_led_connection_crs                        => CONNECTED_TO_tse_mac_status_led_connection_crs,                        --       tse_mac_status_led_connection.crs
			tse_mac_status_led_connection_link                       => CONNECTED_TO_tse_mac_status_led_connection_link,                       --                                    .link
			tse_mac_status_led_connection_panel_link                 => CONNECTED_TO_tse_mac_status_led_connection_panel_link,                 --                                    .panel_link
			tse_mac_status_led_connection_col                        => CONNECTED_TO_tse_mac_status_led_connection_col,                        --                                    .col
			tse_mac_status_led_connection_an                         => CONNECTED_TO_tse_mac_status_led_connection_an,                         --                                    .an
			tse_mac_status_led_connection_char_err                   => CONNECTED_TO_tse_mac_status_led_connection_char_err,                   --                                    .char_err
			tse_mac_status_led_connection_disp_err                   => CONNECTED_TO_tse_mac_status_led_connection_disp_err                    --                                    .disp_err
		);

